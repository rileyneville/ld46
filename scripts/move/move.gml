///@description requires hsp vsp and radius
var hmv = 0;
while(hmv < abs(hsp)) {
	var mv = min(1,abs(hsp)-hmv)*sign(hsp);
	if ((collision_circle(x+mv,y,radius,oImpassable,false,true) == noone) && (x+mv > global.room_min_x) && (x+mv < global.room_max_x)){
		hmv += abs(mv);
		x += mv;
	} else {
		hsp = -hsp;
		break;
	}
}

var vmv = 0;
while(vmv < abs(vsp)) {
	var mv = min(1,abs(vsp)-vmv)*sign(vsp);
	if ((collision_circle(x,y+mv,radius,oImpassable,false,true) == noone) && (y+mv > global.room_min_y) && (y+mv < global.room_max_y)){
		vmv += abs(mv);
		y += mv;
	} else {
		vsp = -vsp;
		break;
	}
}