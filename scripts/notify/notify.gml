with (oNotification) {
	timer = min(timer,30);
}
var notification = instance_create_depth(x,y,depth,oNotification);
notification.text = argument0;