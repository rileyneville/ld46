///draw_shadow(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,height);
///@param sprite_index
///@param image_index
///@param x
///@param y
///@param image_xscale
///@param image_yscale
///@param image_angle
///@param height
var dx = argument2;
var dy = argument3;
var light_distance = point_distance(dx,dy,global.light_x,global.light_y);
if (light_distance > 80) {
	return;
}
var height = argument7;
var light_height_offset =  global.light_height-argument7;
var theta = arctan(light_distance/light_height_offset);
var shadow_offset = tan(theta)*height;
var light_dir = point_direction(global.light_x,global.light_y,dx,dy);
var step = 2;
var scale = 1;
for (var i = step; i < shadow_offset; i+= step;) {
	draw_sprite_ext(argument0,argument1,round(dx+lengthdir_x(i,light_dir)),round(dy+lengthdir_y(i,light_dir)),
	argument4*scale,argument5*scale,argument6,c_black,1);
	if (i >= shadow_offset - 20) {
		scale -= 0.05;
		if (scale < 0.1) {
			return;
		}
	}
}
draw_sprite_ext(argument0,argument1,round(dx+lengthdir_x(shadow_offset,light_dir)),round(dy+lengthdir_y(shadow_offset,light_dir)),
argument4*scale,argument5*scale,argument6,c_black,1);