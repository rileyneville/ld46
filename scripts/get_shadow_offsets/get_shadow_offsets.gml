///@param x
///@param y
///@param height
var dx = argument0;
var dy = argument1;
var height = argument2;

var light_distance = point_distance(dx,dy,global.light_x,global.light_y);
var light_height_offset =  global.light_height-height;
var theta = arctan(light_distance/light_height_offset);
var shadow_offset = tan(theta)*height;
var light_dir = point_direction(global.light_x,global.light_y,dx,dy);
dx += lengthdir_x(shadow_offset,light_dir);
dy += lengthdir_y(shadow_offset,light_dir);


return [dx,dy];