///@param value
///@param angle_limit_2
///@param angle_limit_2
var angle = normalise_angle(argument0);
var minbound = argument1;
var maxbound = argument2;

var n_min = normalise_angle(minbound-angle);
var n_max = normalise_angle(maxbound-angle);

if (n_min <= 0 && n_max >= 0)
{
    return angle;
}
if (abs(n_min) < abs(n_max)) {
    return minbound;
}
return maxbound;