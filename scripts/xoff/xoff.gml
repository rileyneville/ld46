/// @description given an x and y offset, returns the x offset based on image angle.
///@param xoffset
///@param yoffset
///@param [angle]
var xx = argument[0];
var yy = argument[1];
if (argument_count > 2) {
	var dd = argument[2];
} else {
	dd = image_angle;
}
return lengthdir_x(xx,dd) + lengthdir_x(yy,dd-90);