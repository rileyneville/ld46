//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float gamma;

void main()
{
	vec4 base_col = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );	
	float val = base_col.r+base_col.g+base_col.b;
	if (val < 0.15) {
		base_col.b += (0.15-val)*0.25;
	}
	if (val < 0.25) {
		base_col.g += (0.25-val)*0.15;
	}
	base_col.rgb = pow(base_col.rgb,vec3(1./gamma));
	base_col.a = 1.;
	
	gl_FragColor = base_col;
}
