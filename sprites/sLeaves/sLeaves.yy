{
    "id": "e116a67e-2b9f-42ca-9093-6bf654636c63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLeaves",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 12,
    "bbox_right": 19,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66346dd7-aad8-423f-ba22-1e3c68e22363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e116a67e-2b9f-42ca-9093-6bf654636c63",
            "compositeImage": {
                "id": "6efd990c-a8c5-4b91-a2a1-b19fd5f7ba3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66346dd7-aad8-423f-ba22-1e3c68e22363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5d72d36-fa80-4f0d-a446-135d60eb07fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66346dd7-aad8-423f-ba22-1e3c68e22363",
                    "LayerId": "4f25ff7c-0504-4a87-a380-77bbcd5520e6"
                }
            ]
        },
        {
            "id": "cfe4ccad-6184-465f-9c33-4206eb5ae735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e116a67e-2b9f-42ca-9093-6bf654636c63",
            "compositeImage": {
                "id": "34dc84b9-5233-4c46-aa6e-e72cd308edcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfe4ccad-6184-465f-9c33-4206eb5ae735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "588abfe6-ea15-47ad-a29a-47e9a7d9703f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfe4ccad-6184-465f-9c33-4206eb5ae735",
                    "LayerId": "4f25ff7c-0504-4a87-a380-77bbcd5520e6"
                }
            ]
        },
        {
            "id": "aaa0729c-3131-419e-ade4-301a685f870b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e116a67e-2b9f-42ca-9093-6bf654636c63",
            "compositeImage": {
                "id": "3d3a522a-e50f-4e00-b22d-7210baa08a3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaa0729c-3131-419e-ade4-301a685f870b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5470cf39-6924-4a99-bd41-36455067c9eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaa0729c-3131-419e-ade4-301a685f870b",
                    "LayerId": "4f25ff7c-0504-4a87-a380-77bbcd5520e6"
                }
            ]
        },
        {
            "id": "586d7545-48af-47bd-bbb1-0f590b7e58b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e116a67e-2b9f-42ca-9093-6bf654636c63",
            "compositeImage": {
                "id": "ffe8923f-9d19-4d67-94e4-b5a311a81221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "586d7545-48af-47bd-bbb1-0f590b7e58b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff2cf695-b289-4563-ab87-2b40b0048c8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "586d7545-48af-47bd-bbb1-0f590b7e58b8",
                    "LayerId": "4f25ff7c-0504-4a87-a380-77bbcd5520e6"
                }
            ]
        },
        {
            "id": "e20f0fca-9991-4855-8920-fe97788ca6f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e116a67e-2b9f-42ca-9093-6bf654636c63",
            "compositeImage": {
                "id": "981bf262-8559-43ca-a908-21a710da4576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e20f0fca-9991-4855-8920-fe97788ca6f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dfa793c-a88c-4323-a65f-cbe076c838c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e20f0fca-9991-4855-8920-fe97788ca6f9",
                    "LayerId": "4f25ff7c-0504-4a87-a380-77bbcd5520e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4f25ff7c-0504-4a87-a380-77bbcd5520e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e116a67e-2b9f-42ca-9093-6bf654636c63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}