{
    "id": "5da087c7-b88f-4d4b-ae4d-ed67c588abe1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMonsterPart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 16,
    "bbox_right": 143,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6590b93-8e0a-444b-8dc2-04f91d2c985d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5da087c7-b88f-4d4b-ae4d-ed67c588abe1",
            "compositeImage": {
                "id": "c432f530-e60d-4e38-b88b-c9b90fe51de6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6590b93-8e0a-444b-8dc2-04f91d2c985d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7fe1ffb-d7b3-45d3-8fee-bdf4df03d65e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6590b93-8e0a-444b-8dc2-04f91d2c985d",
                    "LayerId": "e838b9fe-d0f7-4e27-b17e-89640b805b03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "e838b9fe-d0f7-4e27-b17e-89640b805b03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5da087c7-b88f-4d4b-ae4d-ed67c588abe1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 80
}