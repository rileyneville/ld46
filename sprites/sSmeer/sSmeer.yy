{
    "id": "4472982e-ba69-48b8-af27-6da505bc8202",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSmeer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 13,
    "bbox_right": 49,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b74a0601-251d-4abf-835d-92564eb86be6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4472982e-ba69-48b8-af27-6da505bc8202",
            "compositeImage": {
                "id": "2189b53b-1495-45d0-8dc6-ec7cafe6d176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b74a0601-251d-4abf-835d-92564eb86be6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5374eb85-bc3b-446d-a3b5-df92684801bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b74a0601-251d-4abf-835d-92564eb86be6",
                    "LayerId": "86b19434-1b5a-43b9-9088-ef06a4848a19"
                },
                {
                    "id": "65f0e66f-9338-496c-9d29-c2f9db147b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b74a0601-251d-4abf-835d-92564eb86be6",
                    "LayerId": "b556b7de-505b-4b20-b4d5-42438867958e"
                },
                {
                    "id": "fea14cab-5704-4e45-a75a-9aba83549e54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b74a0601-251d-4abf-835d-92564eb86be6",
                    "LayerId": "11f0ced6-5485-4bb1-97d6-42ed740afbbd"
                }
            ]
        },
        {
            "id": "fb4977c6-8e75-4788-a08d-ab4dc8fdabf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4472982e-ba69-48b8-af27-6da505bc8202",
            "compositeImage": {
                "id": "55cabda9-c2da-4360-929c-2192c0cb49f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb4977c6-8e75-4788-a08d-ab4dc8fdabf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbbb7980-d4fd-447a-ab5a-7e43a191ce2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb4977c6-8e75-4788-a08d-ab4dc8fdabf2",
                    "LayerId": "86b19434-1b5a-43b9-9088-ef06a4848a19"
                },
                {
                    "id": "220a0138-96fe-426f-9f57-e663076065d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb4977c6-8e75-4788-a08d-ab4dc8fdabf2",
                    "LayerId": "b556b7de-505b-4b20-b4d5-42438867958e"
                },
                {
                    "id": "bca706b4-70ba-46a7-8bea-74685a65d910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb4977c6-8e75-4788-a08d-ab4dc8fdabf2",
                    "LayerId": "11f0ced6-5485-4bb1-97d6-42ed740afbbd"
                }
            ]
        },
        {
            "id": "d7e54003-a987-44c8-8c55-641450d72d0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4472982e-ba69-48b8-af27-6da505bc8202",
            "compositeImage": {
                "id": "83db0bb4-bf2c-4ff7-bbed-b0aa5efcd8d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7e54003-a987-44c8-8c55-641450d72d0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c919295-740a-412a-a3dc-afb7d28c1e1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7e54003-a987-44c8-8c55-641450d72d0b",
                    "LayerId": "86b19434-1b5a-43b9-9088-ef06a4848a19"
                },
                {
                    "id": "c1d6501c-42f7-4cb1-a367-db5b58583d7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7e54003-a987-44c8-8c55-641450d72d0b",
                    "LayerId": "b556b7de-505b-4b20-b4d5-42438867958e"
                },
                {
                    "id": "06d16125-7c3c-422c-a6b2-f06de8bf3c92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7e54003-a987-44c8-8c55-641450d72d0b",
                    "LayerId": "11f0ced6-5485-4bb1-97d6-42ed740afbbd"
                }
            ]
        },
        {
            "id": "ed84b373-6021-45ee-b8dd-e3744b35c092",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4472982e-ba69-48b8-af27-6da505bc8202",
            "compositeImage": {
                "id": "4647d537-c2ce-4071-9a5e-8b5244feb90f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed84b373-6021-45ee-b8dd-e3744b35c092",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcb7cdc1-0187-4371-9416-de8331675ff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed84b373-6021-45ee-b8dd-e3744b35c092",
                    "LayerId": "86b19434-1b5a-43b9-9088-ef06a4848a19"
                },
                {
                    "id": "f123a2ef-7244-459e-9a9f-adb1f7745f9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed84b373-6021-45ee-b8dd-e3744b35c092",
                    "LayerId": "b556b7de-505b-4b20-b4d5-42438867958e"
                },
                {
                    "id": "73c9100c-e6d2-4e63-a266-ea7b198d3000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed84b373-6021-45ee-b8dd-e3744b35c092",
                    "LayerId": "11f0ced6-5485-4bb1-97d6-42ed740afbbd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "11f0ced6-5485-4bb1-97d6-42ed740afbbd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4472982e-ba69-48b8-af27-6da505bc8202",
            "blendMode": 3,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b556b7de-505b-4b20-b4d5-42438867958e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4472982e-ba69-48b8-af27-6da505bc8202",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "86b19434-1b5a-43b9-9088-ef06a4848a19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4472982e-ba69-48b8-af27-6da505bc8202",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 75,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}