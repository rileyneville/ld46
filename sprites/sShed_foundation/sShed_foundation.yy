{
    "id": "651811a0-70a5-46b8-9061-2c05c3ae53b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShed_foundation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 21,
    "bbox_right": 41,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26b79966-aae4-4795-b3dc-25886a677365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "651811a0-70a5-46b8-9061-2c05c3ae53b9",
            "compositeImage": {
                "id": "a2a8bdcb-0f17-45db-ac1a-522d999a28f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26b79966-aae4-4795-b3dc-25886a677365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44eed86f-8f0a-4910-9507-0ab461811242",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26b79966-aae4-4795-b3dc-25886a677365",
                    "LayerId": "0e224332-9554-4048-9f14-d5ec5442b659"
                },
                {
                    "id": "91d5a793-96fe-4b08-a2a9-66022c4a202f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26b79966-aae4-4795-b3dc-25886a677365",
                    "LayerId": "af5a9137-cdac-4f2a-b94c-26d09c1713df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "af5a9137-cdac-4f2a-b94c-26d09c1713df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "651811a0-70a5-46b8-9061-2c05c3ae53b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0e224332-9554-4048-9f14-d5ec5442b659",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "651811a0-70a5-46b8-9061-2c05c3ae53b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}