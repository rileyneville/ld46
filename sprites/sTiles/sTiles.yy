{
    "id": "eb9d95c2-3141-42f9-9d2a-834900f6d5bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ab7d624-006a-4c97-8ded-06f46979e3a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb9d95c2-3141-42f9-9d2a-834900f6d5bd",
            "compositeImage": {
                "id": "820f6f6c-da1a-4e42-a74a-f1f68fca59c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ab7d624-006a-4c97-8ded-06f46979e3a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3adfce81-a711-4d66-a72b-0259d2d1ac46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ab7d624-006a-4c97-8ded-06f46979e3a7",
                    "LayerId": "b35f8db2-23d4-4c16-9006-5f9b859829b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b35f8db2-23d4-4c16-9006-5f9b859829b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb9d95c2-3141-42f9-9d2a-834900f6d5bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}