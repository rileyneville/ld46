{
    "id": "8df72964-4073-43fe-941b-57aa59dde1c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 21,
    "bbox_right": 41,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d27a2c89-8480-4556-b0a7-d26aab7373b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8df72964-4073-43fe-941b-57aa59dde1c9",
            "compositeImage": {
                "id": "95790a63-b236-486c-8c9e-a29c7eb44d8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d27a2c89-8480-4556-b0a7-d26aab7373b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23b35146-db3f-4b6a-b432-8a2e7635405d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d27a2c89-8480-4556-b0a7-d26aab7373b0",
                    "LayerId": "ce5e08a3-78fe-49e6-bade-4bfe0d93dd14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ce5e08a3-78fe-49e6-bade-4bfe0d93dd14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8df72964-4073-43fe-941b-57aa59dde1c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}