{
    "id": "f111286b-9dad-41d6-a0b2-781e70374071",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGore",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 27,
    "bbox_right": 41,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b89c61d0-c4f3-4fc0-a43b-27f35d9c0639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f111286b-9dad-41d6-a0b2-781e70374071",
            "compositeImage": {
                "id": "5c963627-1cd2-46d8-ab83-1c238198d0a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b89c61d0-c4f3-4fc0-a43b-27f35d9c0639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90b038c8-ae17-435c-b786-07a04dc90536",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b89c61d0-c4f3-4fc0-a43b-27f35d9c0639",
                    "LayerId": "30c733aa-3401-433e-9233-ad6eac69589c"
                },
                {
                    "id": "ab525dc6-883a-4929-8fcc-9bf339753992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b89c61d0-c4f3-4fc0-a43b-27f35d9c0639",
                    "LayerId": "7f097083-ed81-482a-9d21-10b84a16107f"
                }
            ]
        },
        {
            "id": "5e095539-d9e9-4aed-b0fa-c3265459120a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f111286b-9dad-41d6-a0b2-781e70374071",
            "compositeImage": {
                "id": "b7519895-5dc1-44b1-8d8f-4d92f32ff6b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e095539-d9e9-4aed-b0fa-c3265459120a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e6fc258-eb56-4765-b9d1-06bd67620d6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e095539-d9e9-4aed-b0fa-c3265459120a",
                    "LayerId": "30c733aa-3401-433e-9233-ad6eac69589c"
                },
                {
                    "id": "b3664f17-9cd3-4df9-9aac-3f22c0c4b77d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e095539-d9e9-4aed-b0fa-c3265459120a",
                    "LayerId": "7f097083-ed81-482a-9d21-10b84a16107f"
                }
            ]
        },
        {
            "id": "e91a6a12-872b-4985-a71d-cd1c46617fa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f111286b-9dad-41d6-a0b2-781e70374071",
            "compositeImage": {
                "id": "cc6d50c0-3158-4ec8-991b-01a9088d3c87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e91a6a12-872b-4985-a71d-cd1c46617fa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fbbdb00-7d36-4ecc-b61f-e160d672ba9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e91a6a12-872b-4985-a71d-cd1c46617fa0",
                    "LayerId": "30c733aa-3401-433e-9233-ad6eac69589c"
                },
                {
                    "id": "3ddd74c4-315e-47ac-8cee-2e597435319a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e91a6a12-872b-4985-a71d-cd1c46617fa0",
                    "LayerId": "7f097083-ed81-482a-9d21-10b84a16107f"
                }
            ]
        },
        {
            "id": "efbad560-292b-44ed-a0d8-794d76fefc39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f111286b-9dad-41d6-a0b2-781e70374071",
            "compositeImage": {
                "id": "0d957d92-3e53-43d7-8b11-bb8072ec0124",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efbad560-292b-44ed-a0d8-794d76fefc39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de8e1402-aa54-4f86-84f8-38372b84b1d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efbad560-292b-44ed-a0d8-794d76fefc39",
                    "LayerId": "30c733aa-3401-433e-9233-ad6eac69589c"
                },
                {
                    "id": "cbcb8f81-c104-437c-a163-96f277de66fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efbad560-292b-44ed-a0d8-794d76fefc39",
                    "LayerId": "7f097083-ed81-482a-9d21-10b84a16107f"
                }
            ]
        },
        {
            "id": "bd165f1d-68ab-4f58-9f1a-7cc50d1768a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f111286b-9dad-41d6-a0b2-781e70374071",
            "compositeImage": {
                "id": "1ea7c8dc-0f2b-46e8-b398-831921caaa04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd165f1d-68ab-4f58-9f1a-7cc50d1768a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc0508e-a1fa-41d2-bf51-8070365ef6c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd165f1d-68ab-4f58-9f1a-7cc50d1768a9",
                    "LayerId": "30c733aa-3401-433e-9233-ad6eac69589c"
                },
                {
                    "id": "0aff2391-7ae4-4bfa-81ed-8b32649e8d9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd165f1d-68ab-4f58-9f1a-7cc50d1768a9",
                    "LayerId": "7f097083-ed81-482a-9d21-10b84a16107f"
                }
            ]
        },
        {
            "id": "ff8eec19-686f-4c1a-8621-41f8d4fbdc63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f111286b-9dad-41d6-a0b2-781e70374071",
            "compositeImage": {
                "id": "b45c6c0f-94a1-42f0-9571-c9302b497e35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff8eec19-686f-4c1a-8621-41f8d4fbdc63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d234965-27b9-43c9-8f09-5602c1e32bce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff8eec19-686f-4c1a-8621-41f8d4fbdc63",
                    "LayerId": "30c733aa-3401-433e-9233-ad6eac69589c"
                },
                {
                    "id": "d0b70f26-d349-4a59-bac1-6fbcb39445ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff8eec19-686f-4c1a-8621-41f8d4fbdc63",
                    "LayerId": "7f097083-ed81-482a-9d21-10b84a16107f"
                }
            ]
        },
        {
            "id": "5d5719f8-f270-461e-b034-05eb0011e0db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f111286b-9dad-41d6-a0b2-781e70374071",
            "compositeImage": {
                "id": "ff698520-f830-4d3a-bf6e-5c8a1c4ce444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d5719f8-f270-461e-b034-05eb0011e0db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5acf3c6-6cdd-4957-8f38-6580fabb52e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d5719f8-f270-461e-b034-05eb0011e0db",
                    "LayerId": "30c733aa-3401-433e-9233-ad6eac69589c"
                },
                {
                    "id": "d9f84304-ff51-4b22-aa7c-3cb7c6b431cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d5719f8-f270-461e-b034-05eb0011e0db",
                    "LayerId": "7f097083-ed81-482a-9d21-10b84a16107f"
                }
            ]
        },
        {
            "id": "6c86c4cf-db2b-4c35-ab02-a4ccf143b941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f111286b-9dad-41d6-a0b2-781e70374071",
            "compositeImage": {
                "id": "a455e865-6524-40a5-a65f-8065f1716e8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c86c4cf-db2b-4c35-ab02-a4ccf143b941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "662a90eb-3c77-47f8-9607-9908704c804f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c86c4cf-db2b-4c35-ab02-a4ccf143b941",
                    "LayerId": "30c733aa-3401-433e-9233-ad6eac69589c"
                },
                {
                    "id": "adf2512d-6a6f-4f62-8e92-705c6542526b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c86c4cf-db2b-4c35-ab02-a4ccf143b941",
                    "LayerId": "7f097083-ed81-482a-9d21-10b84a16107f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "30c733aa-3401-433e-9233-ad6eac69589c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f111286b-9dad-41d6-a0b2-781e70374071",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7f097083-ed81-482a-9d21-10b84a16107f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f111286b-9dad-41d6-a0b2-781e70374071",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 75,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}