{
    "id": "409e33ae-ac2a-4f36-b034-fb26ed6afcff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sArrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 1,
    "bbox_right": 5,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c70a81a-287b-4bfd-9eb8-0ce2684973a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409e33ae-ac2a-4f36-b034-fb26ed6afcff",
            "compositeImage": {
                "id": "1ba8c73b-9f7a-4922-b3b6-e3b9840f8fe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c70a81a-287b-4bfd-9eb8-0ce2684973a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73e5cbd1-9e52-46c4-8270-a24966921a8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c70a81a-287b-4bfd-9eb8-0ce2684973a6",
                    "LayerId": "a4ba1ca0-11dd-4078-ac4d-52b859d45fa6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a4ba1ca0-11dd-4078-ac4d-52b859d45fa6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "409e33ae-ac2a-4f36-b034-fb26ed6afcff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}