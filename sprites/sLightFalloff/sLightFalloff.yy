{
    "id": "f3403560-d96a-4f62-88e0-7ee39fd94133",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLightFalloff",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 639,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8351d6f4-7781-4370-a381-30cfb621a184",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3403560-d96a-4f62-88e0-7ee39fd94133",
            "compositeImage": {
                "id": "02d1b859-b228-4c11-9d3c-d9df8ea99059",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8351d6f4-7781-4370-a381-30cfb621a184",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "360769c4-4171-496e-b4b9-c88aa92afeb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8351d6f4-7781-4370-a381-30cfb621a184",
                    "LayerId": "7c19e704-5f40-474a-a4da-5e743e1e3428"
                },
                {
                    "id": "55e2372a-047f-4a78-81d3-1b72ccf3461b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8351d6f4-7781-4370-a381-30cfb621a184",
                    "LayerId": "3408a126-c1c7-457b-a9d7-f5a80f550754"
                },
                {
                    "id": "f6a14351-bd75-4afe-af1e-6c1937b26a29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8351d6f4-7781-4370-a381-30cfb621a184",
                    "LayerId": "1868ca2c-0c95-48c9-8e3a-baac6c2598e7"
                },
                {
                    "id": "e9400ff1-3149-40b5-8d42-d3fa3e14f8c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8351d6f4-7781-4370-a381-30cfb621a184",
                    "LayerId": "9c2913e5-dfec-4042-8fcf-8627559f4635"
                }
            ]
        },
        {
            "id": "5f25b13f-2d4f-4a59-a42d-8950d3e890a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3403560-d96a-4f62-88e0-7ee39fd94133",
            "compositeImage": {
                "id": "3876ab10-c04a-4fce-9f14-d2f2130715c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f25b13f-2d4f-4a59-a42d-8950d3e890a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9908a764-2347-4d56-a867-fca83ea0778e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f25b13f-2d4f-4a59-a42d-8950d3e890a9",
                    "LayerId": "7c19e704-5f40-474a-a4da-5e743e1e3428"
                },
                {
                    "id": "303d455a-45da-49d9-91cf-656a6af7da04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f25b13f-2d4f-4a59-a42d-8950d3e890a9",
                    "LayerId": "3408a126-c1c7-457b-a9d7-f5a80f550754"
                },
                {
                    "id": "99674538-0b4b-4d97-833d-03099f71e8bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f25b13f-2d4f-4a59-a42d-8950d3e890a9",
                    "LayerId": "1868ca2c-0c95-48c9-8e3a-baac6c2598e7"
                },
                {
                    "id": "01275ece-afd1-46fd-9223-96124b9ac836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f25b13f-2d4f-4a59-a42d-8950d3e890a9",
                    "LayerId": "9c2913e5-dfec-4042-8fcf-8627559f4635"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 640,
    "layers": [
        {
            "id": "7c19e704-5f40-474a-a4da-5e743e1e3428",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3403560-d96a-4f62-88e0-7ee39fd94133",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "9c2913e5-dfec-4042-8fcf-8627559f4635",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3403560-d96a-4f62-88e0-7ee39fd94133",
            "blendMode": 2,
            "isLocked": false,
            "name": "Layer 3 (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3408a126-c1c7-457b-a9d7-f5a80f550754",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3403560-d96a-4f62-88e0-7ee39fd94133",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "1868ca2c-0c95-48c9-8e3a-baac6c2598e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3403560-d96a-4f62-88e0-7ee39fd94133",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 320
}