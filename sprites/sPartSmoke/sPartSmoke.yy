{
    "id": "b070b14e-9f9d-4020-b11e-faa9d0dec26b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPartSmoke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 5,
    "bbox_right": 11,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa1161e1-7cf0-43df-ba96-709564009c90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b070b14e-9f9d-4020-b11e-faa9d0dec26b",
            "compositeImage": {
                "id": "e4ca0cb3-6a35-4b32-b807-61349fe00a44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa1161e1-7cf0-43df-ba96-709564009c90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "532fa319-3b56-413c-b977-59d23883e02e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa1161e1-7cf0-43df-ba96-709564009c90",
                    "LayerId": "7019d49f-d98d-40e2-8d15-375098e1866c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7019d49f-d98d-40e2-8d15-375098e1866c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b070b14e-9f9d-4020-b11e-faa9d0dec26b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}