{
    "id": "c9df3da8-f1de-4101-b655-315d32336bef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "570a47dd-bac8-4ea8-bdd8-58d4f07f9100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9df3da8-f1de-4101-b655-315d32336bef",
            "compositeImage": {
                "id": "46b46f16-e312-44c9-8f77-97d96001c7c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "570a47dd-bac8-4ea8-bdd8-58d4f07f9100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfe92dc8-9af0-4f7b-b0b7-bc2db2e2ca20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "570a47dd-bac8-4ea8-bdd8-58d4f07f9100",
                    "LayerId": "6044dd2c-83b6-4e7e-8b4c-3b5e444f76eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6044dd2c-83b6-4e7e-8b4c-3b5e444f76eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9df3da8-f1de-4101-b655-315d32336bef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}