{
    "id": "2c3d01ea-4242-4026-8b4e-e88797fc39bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunny_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac3cb810-cbc3-4fe8-8cd8-e0170b6f3790",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c3d01ea-4242-4026-8b4e-e88797fc39bd",
            "compositeImage": {
                "id": "079e3000-a123-4456-99b4-c9432dd0a490",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac3cb810-cbc3-4fe8-8cd8-e0170b6f3790",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1a1a618-8a52-423a-ae91-fbc02eb121de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac3cb810-cbc3-4fe8-8cd8-e0170b6f3790",
                    "LayerId": "061907ab-32cb-4443-844d-15c84e7d0bd8"
                }
            ]
        },
        {
            "id": "183fb2c1-c4e7-4c1b-adc3-3197c37d84c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c3d01ea-4242-4026-8b4e-e88797fc39bd",
            "compositeImage": {
                "id": "c3fbbc1b-529a-4be5-82cd-caf1802358ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "183fb2c1-c4e7-4c1b-adc3-3197c37d84c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56c08493-b791-4d33-9757-49c5ac52565b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "183fb2c1-c4e7-4c1b-adc3-3197c37d84c6",
                    "LayerId": "061907ab-32cb-4443-844d-15c84e7d0bd8"
                }
            ]
        },
        {
            "id": "121a531c-299b-4809-8467-eeefb71af5cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c3d01ea-4242-4026-8b4e-e88797fc39bd",
            "compositeImage": {
                "id": "e86fb6c0-62e3-44fa-a9a5-f6868028b824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "121a531c-299b-4809-8467-eeefb71af5cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ed7a4c2-80ec-447e-abfb-f8a351312a72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "121a531c-299b-4809-8467-eeefb71af5cd",
                    "LayerId": "061907ab-32cb-4443-844d-15c84e7d0bd8"
                }
            ]
        },
        {
            "id": "502552e7-835d-4316-9aa3-f2a56d07afd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c3d01ea-4242-4026-8b4e-e88797fc39bd",
            "compositeImage": {
                "id": "4b21fb5d-caf5-4eb4-ac56-acf6257ea666",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "502552e7-835d-4316-9aa3-f2a56d07afd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c14a7a7d-a1c0-498f-a57d-83f71523bf95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "502552e7-835d-4316-9aa3-f2a56d07afd2",
                    "LayerId": "061907ab-32cb-4443-844d-15c84e7d0bd8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "061907ab-32cb-4443-844d-15c84e7d0bd8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c3d01ea-4242-4026-8b4e-e88797fc39bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}