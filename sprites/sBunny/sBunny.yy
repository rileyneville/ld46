{
    "id": "11708ef0-5dce-4af5-8587-c0840ebf6fe0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunny",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 10,
    "bbox_right": 21,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf783862-801d-4e46-90e9-9991d76c038c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11708ef0-5dce-4af5-8587-c0840ebf6fe0",
            "compositeImage": {
                "id": "67528f21-aa25-4599-a736-c6d01f29a47f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf783862-801d-4e46-90e9-9991d76c038c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a82ae0a-f45c-4db8-a40c-cb3078785f83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf783862-801d-4e46-90e9-9991d76c038c",
                    "LayerId": "4a07cbdc-86b1-4573-a9c4-baa2078a3b1f"
                }
            ]
        },
        {
            "id": "5fc2a9d5-6176-42b5-8256-c5237ddb568c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11708ef0-5dce-4af5-8587-c0840ebf6fe0",
            "compositeImage": {
                "id": "de9528a9-5006-4249-9dbd-4606bd99b108",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fc2a9d5-6176-42b5-8256-c5237ddb568c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34521ea6-3cb3-4ab2-b782-4e057c96c2d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fc2a9d5-6176-42b5-8256-c5237ddb568c",
                    "LayerId": "4a07cbdc-86b1-4573-a9c4-baa2078a3b1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4a07cbdc-86b1-4573-a9c4-baa2078a3b1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11708ef0-5dce-4af5-8587-c0840ebf6fe0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}