{
    "id": "55719963-9dff-4169-b844-026055656360",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMorsel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a401c828-4b8b-4ca6-9386-fd860010f2d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55719963-9dff-4169-b844-026055656360",
            "compositeImage": {
                "id": "c2c96023-b410-4c3e-a3da-ae3aeb34e7bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a401c828-4b8b-4ca6-9386-fd860010f2d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af2a7022-909e-4085-8e35-ecd8e45757da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a401c828-4b8b-4ca6-9386-fd860010f2d7",
                    "LayerId": "41b9e919-cb43-41d8-be7c-3ec2cfda18b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "41b9e919-cb43-41d8-be7c-3ec2cfda18b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55719963-9dff-4169-b844-026055656360",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}