{
    "id": "094480c2-fb73-4a59-8ac2-dd2ca81eaa80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSplinters",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d86b0349-4d6a-42fd-a032-91d005e065b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "094480c2-fb73-4a59-8ac2-dd2ca81eaa80",
            "compositeImage": {
                "id": "4404a422-f0b5-4db0-b33f-65d2ed6b468d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d86b0349-4d6a-42fd-a032-91d005e065b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98f57b64-297d-44fe-99dd-a8523203c9a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d86b0349-4d6a-42fd-a032-91d005e065b7",
                    "LayerId": "e58ebaa9-986f-4360-b8af-be65f23c2c48"
                }
            ]
        },
        {
            "id": "b46f456d-d5d1-4be1-af96-1bd3e1dcb70f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "094480c2-fb73-4a59-8ac2-dd2ca81eaa80",
            "compositeImage": {
                "id": "50d4565a-35ba-4e3d-bfe4-0da394ef4e54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b46f456d-d5d1-4be1-af96-1bd3e1dcb70f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "116ac1fb-7a91-4b49-9e83-aaebdb2e8bbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b46f456d-d5d1-4be1-af96-1bd3e1dcb70f",
                    "LayerId": "e58ebaa9-986f-4360-b8af-be65f23c2c48"
                }
            ]
        },
        {
            "id": "7090153a-870f-40db-ab47-2c01e350bd83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "094480c2-fb73-4a59-8ac2-dd2ca81eaa80",
            "compositeImage": {
                "id": "3e53c7c5-5199-4285-a143-e5622fcc1a19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7090153a-870f-40db-ab47-2c01e350bd83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11729d96-7245-4274-947a-1914e4894a15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7090153a-870f-40db-ab47-2c01e350bd83",
                    "LayerId": "e58ebaa9-986f-4360-b8af-be65f23c2c48"
                }
            ]
        },
        {
            "id": "00f4eef5-9c41-491f-b470-f6f85d0c0e43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "094480c2-fb73-4a59-8ac2-dd2ca81eaa80",
            "compositeImage": {
                "id": "ddd05649-284d-4220-afb6-0628b265fbc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00f4eef5-9c41-491f-b470-f6f85d0c0e43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f420f1f4-c5be-40ab-b542-845b38ec9151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00f4eef5-9c41-491f-b470-f6f85d0c0e43",
                    "LayerId": "e58ebaa9-986f-4360-b8af-be65f23c2c48"
                }
            ]
        },
        {
            "id": "cc5225ef-58dd-400d-9100-1579d3578da0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "094480c2-fb73-4a59-8ac2-dd2ca81eaa80",
            "compositeImage": {
                "id": "bb56f82c-e0b2-4998-8027-0dafbf22a2b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc5225ef-58dd-400d-9100-1579d3578da0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09cb3547-5432-4833-97de-5f75439f3a52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc5225ef-58dd-400d-9100-1579d3578da0",
                    "LayerId": "e58ebaa9-986f-4360-b8af-be65f23c2c48"
                }
            ]
        },
        {
            "id": "bab037e4-8c5e-4beb-8c95-68b098388cde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "094480c2-fb73-4a59-8ac2-dd2ca81eaa80",
            "compositeImage": {
                "id": "457d9c47-cd64-4002-8651-304b9284becb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bab037e4-8c5e-4beb-8c95-68b098388cde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "452c6188-da39-4087-a846-94281514319e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bab037e4-8c5e-4beb-8c95-68b098388cde",
                    "LayerId": "e58ebaa9-986f-4360-b8af-be65f23c2c48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "e58ebaa9-986f-4360-b8af-be65f23c2c48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "094480c2-fb73-4a59-8ac2-dd2ca81eaa80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 9
}