{
    "id": "37f18d1f-c5f1-4b85-b5e8-b3469c1fa78f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHay",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3190805a-0b76-4872-90ef-f15a882989f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37f18d1f-c5f1-4b85-b5e8-b3469c1fa78f",
            "compositeImage": {
                "id": "9b990802-d495-4a35-a61a-008496548d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3190805a-0b76-4872-90ef-f15a882989f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0929f0c-d619-450f-bf62-642a35c08943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3190805a-0b76-4872-90ef-f15a882989f8",
                    "LayerId": "2f178e6e-b609-43b5-9438-b4238cdf8569"
                }
            ]
        },
        {
            "id": "9462cc69-30b7-496a-b960-e00b6350ebc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37f18d1f-c5f1-4b85-b5e8-b3469c1fa78f",
            "compositeImage": {
                "id": "79da53de-bf24-449a-8691-6b777d6da299",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9462cc69-30b7-496a-b960-e00b6350ebc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdf34262-ec6d-49be-890c-4af9f7d92687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9462cc69-30b7-496a-b960-e00b6350ebc3",
                    "LayerId": "2f178e6e-b609-43b5-9438-b4238cdf8569"
                }
            ]
        },
        {
            "id": "79f8425f-00e7-4e7b-a0ab-3811018d1413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37f18d1f-c5f1-4b85-b5e8-b3469c1fa78f",
            "compositeImage": {
                "id": "dea5477c-e47d-422b-9151-f5f7d625398e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79f8425f-00e7-4e7b-a0ab-3811018d1413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70311f2a-98de-49e3-9495-24bfbbb7fa41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79f8425f-00e7-4e7b-a0ab-3811018d1413",
                    "LayerId": "2f178e6e-b609-43b5-9438-b4238cdf8569"
                }
            ]
        },
        {
            "id": "070e8db0-4b09-4531-bd51-ee2038f34e42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37f18d1f-c5f1-4b85-b5e8-b3469c1fa78f",
            "compositeImage": {
                "id": "d8b9daab-a708-4561-b950-23b7735793b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "070e8db0-4b09-4531-bd51-ee2038f34e42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6300bf02-fb61-486a-aa71-86c0f4800a4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "070e8db0-4b09-4531-bd51-ee2038f34e42",
                    "LayerId": "2f178e6e-b609-43b5-9438-b4238cdf8569"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2f178e6e-b609-43b5-9438-b4238cdf8569",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37f18d1f-c5f1-4b85-b5e8-b3469c1fa78f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}