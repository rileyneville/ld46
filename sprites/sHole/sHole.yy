{
    "id": "bfbe9872-1d14-4b75-a9c0-0e515f6760ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 8,
    "bbox_right": 24,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96637878-94b1-42b4-844d-ba150b50d995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfbe9872-1d14-4b75-a9c0-0e515f6760ad",
            "compositeImage": {
                "id": "5b7e2998-bc6c-42c8-a763-092418392f04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96637878-94b1-42b4-844d-ba150b50d995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d061ea91-c7f5-4417-9b73-90c09c666324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96637878-94b1-42b4-844d-ba150b50d995",
                    "LayerId": "a700dcb2-beda-4f9d-bc61-7c62bdf68ee7"
                },
                {
                    "id": "0c5ae9da-c88e-41e8-9d7b-541c28159bbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96637878-94b1-42b4-844d-ba150b50d995",
                    "LayerId": "67a3e5ed-9d4a-4f97-9cbf-b05c6c7f56ad"
                },
                {
                    "id": "756d228d-5d68-4bc5-8b6d-0c0562a8777c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96637878-94b1-42b4-844d-ba150b50d995",
                    "LayerId": "9283e519-adea-4a73-b828-16a36db9ce08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9283e519-adea-4a73-b828-16a36db9ce08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfbe9872-1d14-4b75-a9c0-0e515f6760ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "67a3e5ed-9d4a-4f97-9cbf-b05c6c7f56ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfbe9872-1d14-4b75-a9c0-0e515f6760ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a700dcb2-beda-4f9d-bc61-7c62bdf68ee7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfbe9872-1d14-4b75-a9c0-0e515f6760ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}