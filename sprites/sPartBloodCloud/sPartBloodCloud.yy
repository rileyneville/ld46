{
    "id": "78f019b8-8620-4a5a-93b9-d20e3ebeaef6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPartBloodCloud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11cc9082-46b0-4ddd-af35-06cad6a9aa1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f019b8-8620-4a5a-93b9-d20e3ebeaef6",
            "compositeImage": {
                "id": "217e6c01-4701-4dc4-9c6d-03228ce05ebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11cc9082-46b0-4ddd-af35-06cad6a9aa1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90df83af-6de1-45c4-9207-98dd5861651c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11cc9082-46b0-4ddd-af35-06cad6a9aa1c",
                    "LayerId": "6bc9cb26-cc10-45f3-be4e-0607a53b2526"
                },
                {
                    "id": "c214fafe-ccc4-4d77-97ce-e72721d11fed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11cc9082-46b0-4ddd-af35-06cad6a9aa1c",
                    "LayerId": "a7914909-af0a-453c-8ddd-87e8484f96f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a7914909-af0a-453c-8ddd-87e8484f96f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78f019b8-8620-4a5a-93b9-d20e3ebeaef6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6bc9cb26-cc10-45f3-be4e-0607a53b2526",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78f019b8-8620-4a5a-93b9-d20e3ebeaef6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}