{
    "id": "e955b8e9-170c-46fe-a77a-d8e7861a7380",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05f0b7a2-9417-4371-92a7-fc5f52af33b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e955b8e9-170c-46fe-a77a-d8e7861a7380",
            "compositeImage": {
                "id": "81f7c686-b0f3-4f0e-88b1-06d1f5cd2292",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05f0b7a2-9417-4371-92a7-fc5f52af33b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57635e1e-2ef9-4927-b58c-f1aa48dac4a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05f0b7a2-9417-4371-92a7-fc5f52af33b7",
                    "LayerId": "9e7dd29f-f5e2-4aa6-b647-12d9dfff4622"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "9e7dd29f-f5e2-4aa6-b647-12d9dfff4622",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e955b8e9-170c-46fe-a77a-d8e7861a7380",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 9
}