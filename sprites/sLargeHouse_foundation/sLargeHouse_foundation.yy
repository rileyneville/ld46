{
    "id": "00423f01-e5fe-4e29-a39c-99cbe9c6a8f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLargeHouse_foundation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 37,
    "bbox_right": 91,
    "bbox_top": 45,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "faab2da4-65d2-49c7-9622-c7354efa4583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00423f01-e5fe-4e29-a39c-99cbe9c6a8f2",
            "compositeImage": {
                "id": "075aff6b-9277-4121-aba0-0cd99a04ac84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faab2da4-65d2-49c7-9622-c7354efa4583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19f1d52e-91d7-4584-a30f-3236b6eabf51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faab2da4-65d2-49c7-9622-c7354efa4583",
                    "LayerId": "4e80f7fe-a8c9-41b2-9e06-d92344f2c369"
                },
                {
                    "id": "1eb9a3d5-c7aa-4ffa-9cf9-bf830e0393b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faab2da4-65d2-49c7-9622-c7354efa4583",
                    "LayerId": "b4f4f649-ca8b-473b-ab60-4145dbf037e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b4f4f649-ca8b-473b-ab60-4145dbf037e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00423f01-e5fe-4e29-a39c-99cbe9c6a8f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4e80f7fe-a8c9-41b2-9e06-d92344f2c369",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00423f01-e5fe-4e29-a39c-99cbe9c6a8f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}