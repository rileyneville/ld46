{
    "id": "554736be-3367-4af3-a0a4-e0e6f1778643",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSmallHouse_foundation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 13,
    "bbox_right": 49,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5e9c469-1331-4c07-9427-1a96238ddd27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "554736be-3367-4af3-a0a4-e0e6f1778643",
            "compositeImage": {
                "id": "dcb8d3ba-cf19-4992-aec2-c344a948d874",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5e9c469-1331-4c07-9427-1a96238ddd27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad9d7fdb-aaa1-429a-a8de-9e69f6f54422",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5e9c469-1331-4c07-9427-1a96238ddd27",
                    "LayerId": "0a64bb71-c241-45e3-b5fc-cb955a28f948"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0a64bb71-c241-45e3-b5fc-cb955a28f948",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "554736be-3367-4af3-a0a4-e0e6f1778643",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}