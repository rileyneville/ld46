{
    "id": "35b1ae78-b439-4b3e-9590-3e55560c2f77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPartFlame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 6,
    "bbox_right": 8,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d04623f-daf4-4eea-9e6d-342d911267ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35b1ae78-b439-4b3e-9590-3e55560c2f77",
            "compositeImage": {
                "id": "bb3b81de-1677-4bdc-9233-b7af6e9fb038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d04623f-daf4-4eea-9e6d-342d911267ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11b311cb-fa52-4291-8fd8-f5a293e52572",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d04623f-daf4-4eea-9e6d-342d911267ff",
                    "LayerId": "7a62f150-905b-40d9-8b1a-b9880d5f6178"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7a62f150-905b-40d9-8b1a-b9880d5f6178",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35b1ae78-b439-4b3e-9590-3e55560c2f77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}