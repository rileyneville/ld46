{
    "id": "58977fb7-bb31-4cbd-8b6c-46e39b5c6566",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHayBale",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "381b5f3e-7bfc-43be-8c34-811951fa4946",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58977fb7-bb31-4cbd-8b6c-46e39b5c6566",
            "compositeImage": {
                "id": "8a1ad245-2516-4f94-beed-0f089e9a40b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "381b5f3e-7bfc-43be-8c34-811951fa4946",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0a46e53-6cd3-4d76-9f34-86c1810baff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "381b5f3e-7bfc-43be-8c34-811951fa4946",
                    "LayerId": "0d02832e-362b-464e-8799-91aaae2dd344"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0d02832e-362b-464e-8799-91aaae2dd344",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58977fb7-bb31-4cbd-8b6c-46e39b5c6566",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}