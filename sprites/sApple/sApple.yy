{
    "id": "e6d55551-e823-4893-b568-c76b46c8579d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sApple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 3,
    "bbox_right": 11,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5203ce9-8051-4a25-8066-7b5aff7e64d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6d55551-e823-4893-b568-c76b46c8579d",
            "compositeImage": {
                "id": "9048ee5f-393f-4274-ae43-897e6db614d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5203ce9-8051-4a25-8066-7b5aff7e64d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87b86c02-7592-4a8d-9a3f-15b5993ade60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5203ce9-8051-4a25-8066-7b5aff7e64d5",
                    "LayerId": "d754cd77-8886-44e9-9b2f-e7447165bd9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d754cd77-8886-44e9-9b2f-e7447165bd9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6d55551-e823-4893-b568-c76b46c8579d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}