{
    "id": "6627b380-6554-4a5f-b3f7-568631d9264d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSheep",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ccd0bf2-4714-45dc-893f-5c5a24bce12c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6627b380-6554-4a5f-b3f7-568631d9264d",
            "compositeImage": {
                "id": "0c9d67e6-ae40-4621-981e-6d1bf16147fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ccd0bf2-4714-45dc-893f-5c5a24bce12c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba672fc9-d585-42b9-a7b8-9e47e87d4606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ccd0bf2-4714-45dc-893f-5c5a24bce12c",
                    "LayerId": "c3fca923-24c5-4ad4-bfe5-d221bf9bd1bb"
                }
            ]
        },
        {
            "id": "97b96e15-2eb7-4a19-8882-6b222ee96cba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6627b380-6554-4a5f-b3f7-568631d9264d",
            "compositeImage": {
                "id": "be7ee8e2-f103-4dac-8842-ec0df55681d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97b96e15-2eb7-4a19-8882-6b222ee96cba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "541eeec1-745f-4d17-b3dc-df1e9162c0db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97b96e15-2eb7-4a19-8882-6b222ee96cba",
                    "LayerId": "c3fca923-24c5-4ad4-bfe5-d221bf9bd1bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c3fca923-24c5-4ad4-bfe5-d221bf9bd1bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6627b380-6554-4a5f-b3f7-568631d9264d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}