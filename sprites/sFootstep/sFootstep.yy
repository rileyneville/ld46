{
    "id": "6ed5e8ce-b17a-4265-ad54-4eefb26ae128",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFootstep",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 7,
    "bbox_right": 8,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "286a9ef5-ce35-4d74-9c90-0459453ab64d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ed5e8ce-b17a-4265-ad54-4eefb26ae128",
            "compositeImage": {
                "id": "ff114714-89c7-44ca-b9fb-8cd26202e9cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "286a9ef5-ce35-4d74-9c90-0459453ab64d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c01ac869-ebb0-4fc1-8754-1deda4876546",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "286a9ef5-ce35-4d74-9c90-0459453ab64d",
                    "LayerId": "c143177d-3d49-453d-a1ce-9ef64ca2ec9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c143177d-3d49-453d-a1ce-9ef64ca2ec9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ed5e8ce-b17a-4265-ad54-4eefb26ae128",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 9
}