{
    "id": "2e08845a-82ce-4188-b7d8-4b4814b38670",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 13,
    "bbox_right": 18,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "868487f2-2da0-428c-95b9-a580b9cae82a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e08845a-82ce-4188-b7d8-4b4814b38670",
            "compositeImage": {
                "id": "19b4a833-2744-41db-9143-6df044324d97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "868487f2-2da0-428c-95b9-a580b9cae82a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fead950e-31c5-46a4-af15-64f34ef3a8c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "868487f2-2da0-428c-95b9-a580b9cae82a",
                    "LayerId": "d54e041e-7f34-468d-b97d-70995d34e563"
                }
            ]
        },
        {
            "id": "855f2ee4-d5fa-4e60-a2d6-fe134598b579",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e08845a-82ce-4188-b7d8-4b4814b38670",
            "compositeImage": {
                "id": "c0fe3ff0-0b30-4d5b-9750-840ea343eac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "855f2ee4-d5fa-4e60-a2d6-fe134598b579",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06fbc7b3-a336-4109-af5a-7e069b43421c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "855f2ee4-d5fa-4e60-a2d6-fe134598b579",
                    "LayerId": "d54e041e-7f34-468d-b97d-70995d34e563"
                }
            ]
        },
        {
            "id": "017b7b76-2cbb-41c7-8327-513fe85731ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e08845a-82ce-4188-b7d8-4b4814b38670",
            "compositeImage": {
                "id": "54e1aaf0-ce3f-4cb9-83ee-c7884d8fd68f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "017b7b76-2cbb-41c7-8327-513fe85731ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7ae8f0d-1eaa-434e-bc26-9b3830a760d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "017b7b76-2cbb-41c7-8327-513fe85731ee",
                    "LayerId": "d54e041e-7f34-468d-b97d-70995d34e563"
                }
            ]
        },
        {
            "id": "4b44472c-12db-40d0-b3b1-f34983a62f3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e08845a-82ce-4188-b7d8-4b4814b38670",
            "compositeImage": {
                "id": "534e98e6-79f4-4944-a2e4-6d66e0c2df5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b44472c-12db-40d0-b3b1-f34983a62f3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "632f8c4a-51cf-4059-8216-c2c64f0c25a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b44472c-12db-40d0-b3b1-f34983a62f3f",
                    "LayerId": "d54e041e-7f34-468d-b97d-70995d34e563"
                }
            ]
        },
        {
            "id": "e0885d68-287a-4cb3-9939-efcdd3180d00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e08845a-82ce-4188-b7d8-4b4814b38670",
            "compositeImage": {
                "id": "28fc14f3-0878-444a-8019-d58e6fadf9a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0885d68-287a-4cb3-9939-efcdd3180d00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e8ca5c7-c329-4eaf-b81c-153c4c86bf22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0885d68-287a-4cb3-9939-efcdd3180d00",
                    "LayerId": "d54e041e-7f34-468d-b97d-70995d34e563"
                }
            ]
        },
        {
            "id": "65bf6f9b-b70a-4b8f-a506-f967b6d095b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e08845a-82ce-4188-b7d8-4b4814b38670",
            "compositeImage": {
                "id": "0729ca1c-1c11-4a17-a871-f7dfd0715d29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65bf6f9b-b70a-4b8f-a506-f967b6d095b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ae5fc90-ff3f-44af-a78d-6b326b3c66da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65bf6f9b-b70a-4b8f-a506-f967b6d095b7",
                    "LayerId": "d54e041e-7f34-468d-b97d-70995d34e563"
                }
            ]
        },
        {
            "id": "ba4fc8ac-1d77-4b6b-a50e-ed80ea161f10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e08845a-82ce-4188-b7d8-4b4814b38670",
            "compositeImage": {
                "id": "2967c920-e545-49fc-a630-0ccd78500d0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba4fc8ac-1d77-4b6b-a50e-ed80ea161f10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03cf7b41-1830-4b4c-8061-6590f8572c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba4fc8ac-1d77-4b6b-a50e-ed80ea161f10",
                    "LayerId": "d54e041e-7f34-468d-b97d-70995d34e563"
                }
            ]
        },
        {
            "id": "4b4bcfd3-9520-4e54-91d1-23204cc73b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e08845a-82ce-4188-b7d8-4b4814b38670",
            "compositeImage": {
                "id": "e31482f3-4466-4d1a-81dc-bab8edbfeb9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b4bcfd3-9520-4e54-91d1-23204cc73b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ae7e124-57e5-48ea-88fc-b834d7222c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b4bcfd3-9520-4e54-91d1-23204cc73b58",
                    "LayerId": "d54e041e-7f34-468d-b97d-70995d34e563"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d54e041e-7f34-468d-b97d-70995d34e563",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e08845a-82ce-4188-b7d8-4b4814b38670",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}