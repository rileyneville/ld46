{
    "id": "60fbbe2a-3e0b-43e8-88f5-72797d7be911",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSheep_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc374aec-47f4-422e-b61f-07e233bfe778",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fbbe2a-3e0b-43e8-88f5-72797d7be911",
            "compositeImage": {
                "id": "691b2c50-1135-4d8e-a32c-69e1dc15905a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc374aec-47f4-422e-b61f-07e233bfe778",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "602ff955-6839-4960-936b-1036fb990773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc374aec-47f4-422e-b61f-07e233bfe778",
                    "LayerId": "c44ac989-186a-435a-9778-e1d890cc100a"
                }
            ]
        },
        {
            "id": "12459c3e-3f4c-4265-9e45-51097b028600",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fbbe2a-3e0b-43e8-88f5-72797d7be911",
            "compositeImage": {
                "id": "7282472c-8449-4b41-86e0-84ec6e63b553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12459c3e-3f4c-4265-9e45-51097b028600",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "278075f4-5483-46fa-a69d-411324a9419e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12459c3e-3f4c-4265-9e45-51097b028600",
                    "LayerId": "c44ac989-186a-435a-9778-e1d890cc100a"
                }
            ]
        },
        {
            "id": "79243b0b-293d-475e-8b43-29bde155db47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fbbe2a-3e0b-43e8-88f5-72797d7be911",
            "compositeImage": {
                "id": "280b1a90-3a86-4ff1-b05e-214a1f9211dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79243b0b-293d-475e-8b43-29bde155db47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6db607a1-8b91-4652-b443-2b76207e69bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79243b0b-293d-475e-8b43-29bde155db47",
                    "LayerId": "c44ac989-186a-435a-9778-e1d890cc100a"
                }
            ]
        },
        {
            "id": "dad59e78-8e5b-457e-8ab6-748a66343ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fbbe2a-3e0b-43e8-88f5-72797d7be911",
            "compositeImage": {
                "id": "8b1e8fd0-a642-4639-a0b6-79a7773c95a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dad59e78-8e5b-457e-8ab6-748a66343ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ebf4c09-6ac3-4b64-bac6-220c4bc6af16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dad59e78-8e5b-457e-8ab6-748a66343ce3",
                    "LayerId": "c44ac989-186a-435a-9778-e1d890cc100a"
                }
            ]
        },
        {
            "id": "c2d2fda1-5494-455e-8273-e6177dbcfff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fbbe2a-3e0b-43e8-88f5-72797d7be911",
            "compositeImage": {
                "id": "11514375-4992-41f6-a84d-564fa2e672bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2d2fda1-5494-455e-8273-e6177dbcfff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c63b6f5-8a49-468e-bb54-59a38f4e3e28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2d2fda1-5494-455e-8273-e6177dbcfff4",
                    "LayerId": "c44ac989-186a-435a-9778-e1d890cc100a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c44ac989-186a-435a-9778-e1d890cc100a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60fbbe2a-3e0b-43e8-88f5-72797d7be911",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}