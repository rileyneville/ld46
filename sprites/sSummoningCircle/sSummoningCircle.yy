{
    "id": "87c0b4e6-40b9-4e92-8efd-b02b2af272d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSummoningCircle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 4,
    "bbox_right": 60,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e95fa89d-ccdd-455d-855a-ca59f142d69b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87c0b4e6-40b9-4e92-8efd-b02b2af272d7",
            "compositeImage": {
                "id": "088402c6-1a1b-49f9-9b37-f8eabb798e9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e95fa89d-ccdd-455d-855a-ca59f142d69b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40b47157-3b7c-4617-ada5-8ea64717b93a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e95fa89d-ccdd-455d-855a-ca59f142d69b",
                    "LayerId": "9a9967de-7ddf-4e61-b5b2-7a0ecc26ce88"
                },
                {
                    "id": "6d85c2dd-3bcd-49a0-9ef2-129f5d9b95c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e95fa89d-ccdd-455d-855a-ca59f142d69b",
                    "LayerId": "8d9a4465-e223-4b6d-bbb6-00983b9945a9"
                },
                {
                    "id": "2c27a9e1-65d0-4a2a-b10e-1d75189e58b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e95fa89d-ccdd-455d-855a-ca59f142d69b",
                    "LayerId": "ca8692ae-787d-4878-830f-2ad7e0d9a0c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ca8692ae-787d-4878-830f-2ad7e0d9a0c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87c0b4e6-40b9-4e92-8efd-b02b2af272d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2) (2)",
            "opacity": 75,
            "visible": true
        },
        {
            "id": "8d9a4465-e223-4b6d-bbb6-00983b9945a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87c0b4e6-40b9-4e92-8efd-b02b2af272d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9a9967de-7ddf-4e61-b5b2-7a0ecc26ce88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87c0b4e6-40b9-4e92-8efd-b02b2af272d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}