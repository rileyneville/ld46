{
    "id": "75a6d18a-ac55-4129-be29-44dbaa398142",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChicken_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 10,
    "bbox_right": 21,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e65e1b52-aad0-4387-900d-101cc8c9f049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a6d18a-ac55-4129-be29-44dbaa398142",
            "compositeImage": {
                "id": "3f33b68c-39b2-4fae-b8a0-3711a7e4a4d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e65e1b52-aad0-4387-900d-101cc8c9f049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc7ebe51-88c3-4073-84e9-cd1dfe8d99d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e65e1b52-aad0-4387-900d-101cc8c9f049",
                    "LayerId": "26ca1df6-d67d-4e67-ae75-dde7e0af4eef"
                }
            ]
        },
        {
            "id": "c6fabf59-5de7-497d-a8c3-b6e6d0be6f17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a6d18a-ac55-4129-be29-44dbaa398142",
            "compositeImage": {
                "id": "8d1e1534-f242-400b-8270-873cb40a4f17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6fabf59-5de7-497d-a8c3-b6e6d0be6f17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a02bd9b-3b65-4acf-93d6-7c08508afb67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6fabf59-5de7-497d-a8c3-b6e6d0be6f17",
                    "LayerId": "26ca1df6-d67d-4e67-ae75-dde7e0af4eef"
                }
            ]
        },
        {
            "id": "e0b1478c-4896-4ac8-86f5-dd714ea0a780",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a6d18a-ac55-4129-be29-44dbaa398142",
            "compositeImage": {
                "id": "ef92d309-adc9-4c0d-b814-ea128bda0762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0b1478c-4896-4ac8-86f5-dd714ea0a780",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8eb701e-2f3a-452c-a5ab-6bb916ba351d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0b1478c-4896-4ac8-86f5-dd714ea0a780",
                    "LayerId": "26ca1df6-d67d-4e67-ae75-dde7e0af4eef"
                }
            ]
        },
        {
            "id": "c6545cdc-3e1d-493c-ac00-67796b914a43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a6d18a-ac55-4129-be29-44dbaa398142",
            "compositeImage": {
                "id": "ae8f06f5-ca9d-44b7-8c95-0d94bfc06add",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6545cdc-3e1d-493c-ac00-67796b914a43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88bb5f2b-52a0-490d-a88c-63b974d1b5b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6545cdc-3e1d-493c-ac00-67796b914a43",
                    "LayerId": "26ca1df6-d67d-4e67-ae75-dde7e0af4eef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "26ca1df6-d67d-4e67-ae75-dde7e0af4eef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75a6d18a-ac55-4129-be29-44dbaa398142",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}