{
    "id": "99e9f5ef-15e3-4bf4-a3bf-6575b7ff1a23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSmallHouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 13,
    "bbox_right": 49,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d936e3fb-c093-4bb4-a9d7-bfe3a7376180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99e9f5ef-15e3-4bf4-a3bf-6575b7ff1a23",
            "compositeImage": {
                "id": "a9a4c5e9-ad1b-4a87-9133-71b7458faa5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d936e3fb-c093-4bb4-a9d7-bfe3a7376180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "043b7bbd-a9cf-4790-ab8f-9b451cf23cb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d936e3fb-c093-4bb4-a9d7-bfe3a7376180",
                    "LayerId": "ed967f40-6156-426d-85f0-9c07063920fd"
                },
                {
                    "id": "86e45c14-ffed-438a-a613-33edd03698fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d936e3fb-c093-4bb4-a9d7-bfe3a7376180",
                    "LayerId": "8ecfa6b5-fc9a-443e-9818-583133ee2b2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8ecfa6b5-fc9a-443e-9818-583133ee2b2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99e9f5ef-15e3-4bf4-a3bf-6575b7ff1a23",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ed967f40-6156-426d-85f0-9c07063920fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99e9f5ef-15e3-4bf4-a3bf-6575b7ff1a23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}