{
    "id": "10c80701-0878-4fdc-9d94-93187107c201",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRibs",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20ec6a44-8577-4f16-8275-845ae13bd8e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10c80701-0878-4fdc-9d94-93187107c201",
            "compositeImage": {
                "id": "d1520d29-1efc-446d-bff4-ced6085c9f1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20ec6a44-8577-4f16-8275-845ae13bd8e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "906dfff4-7afe-4d10-80cb-89614a47d4a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20ec6a44-8577-4f16-8275-845ae13bd8e4",
                    "LayerId": "0b66d000-82cc-475d-9850-f84bbdd211f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0b66d000-82cc-475d-9850-f84bbdd211f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10c80701-0878-4fdc-9d94-93187107c201",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}