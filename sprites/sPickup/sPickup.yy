{
    "id": "d10870e2-8f54-4a96-a8a7-5de38663e72d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPickup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2fd9446-1f8c-46a4-9e5e-5816d6c719e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d10870e2-8f54-4a96-a8a7-5de38663e72d",
            "compositeImage": {
                "id": "c51fde40-c1ec-468f-96ee-09634d701cc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2fd9446-1f8c-46a4-9e5e-5816d6c719e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5b4ef13-f5b5-48e1-85ef-57eb3edd30e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2fd9446-1f8c-46a4-9e5e-5816d6c719e2",
                    "LayerId": "d68ef814-a87d-4011-9cdb-cf6821d8d26e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d68ef814-a87d-4011-9cdb-cf6821d8d26e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d10870e2-8f54-4a96-a8a7-5de38663e72d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}