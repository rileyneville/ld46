{
    "id": "75eb64f4-0720-4bd0-8d59-73a6a8a7140e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCrate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ff5119a-f872-43df-b04b-8af22696aa37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75eb64f4-0720-4bd0-8d59-73a6a8a7140e",
            "compositeImage": {
                "id": "e0d2ec15-cab8-4dd1-88b8-12ab0fe4ed83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ff5119a-f872-43df-b04b-8af22696aa37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0166f38-ffe6-427b-b4e6-2e2b9a15dc88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ff5119a-f872-43df-b04b-8af22696aa37",
                    "LayerId": "95484260-737f-42c4-9a6f-72912d5b5277"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "95484260-737f-42c4-9a6f-72912d5b5277",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75eb64f4-0720-4bd0-8d59-73a6a8a7140e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}