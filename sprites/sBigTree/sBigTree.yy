{
    "id": "0e29bea4-04f7-47f1-aba7-c64ae6876eff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBigTree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 1,
    "bbox_right": 58,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d8541a5-f331-46be-9d72-d8aac06cbde4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e29bea4-04f7-47f1-aba7-c64ae6876eff",
            "compositeImage": {
                "id": "8e381412-6851-4b37-ad11-501f7b15dbb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d8541a5-f331-46be-9d72-d8aac06cbde4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e77113a5-b120-438f-8683-d92022235772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d8541a5-f331-46be-9d72-d8aac06cbde4",
                    "LayerId": "4b63ef65-d4ff-4d4c-8394-de094ec0c5cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4b63ef65-d4ff-4d4c-8394-de094ec0c5cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e29bea4-04f7-47f1-aba7-c64ae6876eff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}