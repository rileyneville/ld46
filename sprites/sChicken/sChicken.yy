{
    "id": "b35c74e3-1ae2-4882-9c2b-6db7d8a4ef19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChicken",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 10,
    "bbox_right": 21,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed5c121e-1714-4729-8b8b-d1d3c36af2df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b35c74e3-1ae2-4882-9c2b-6db7d8a4ef19",
            "compositeImage": {
                "id": "cb3abad7-68ac-4227-85f2-ea70faaa6bb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed5c121e-1714-4729-8b8b-d1d3c36af2df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "467daef7-1292-4182-a778-bd4a3f6033a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed5c121e-1714-4729-8b8b-d1d3c36af2df",
                    "LayerId": "f6d74a8a-e9d4-41bb-9570-936fd2a540b3"
                }
            ]
        },
        {
            "id": "446d54db-8b1e-422f-a60f-538e6e60204e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b35c74e3-1ae2-4882-9c2b-6db7d8a4ef19",
            "compositeImage": {
                "id": "bc289de2-a257-459c-a8ee-af97697bcabd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "446d54db-8b1e-422f-a60f-538e6e60204e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cba2dea-e83c-4c82-a19b-96c5d60504cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "446d54db-8b1e-422f-a60f-538e6e60204e",
                    "LayerId": "f6d74a8a-e9d4-41bb-9570-936fd2a540b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f6d74a8a-e9d4-41bb-9570-936fd2a540b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b35c74e3-1ae2-4882-9c2b-6db7d8a4ef19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}