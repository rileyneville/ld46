{
    "id": "bcdbc4ef-0166-441e-9c99-103837f183cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRubble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 5,
    "bbox_right": 8,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a424510f-6f5a-4131-a26c-998e0d1afc11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcdbc4ef-0166-441e-9c99-103837f183cd",
            "compositeImage": {
                "id": "3141bf69-ef72-45f0-8b9a-b14e63d99752",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a424510f-6f5a-4131-a26c-998e0d1afc11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d083350-18d0-4651-9283-eecd69c3f863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a424510f-6f5a-4131-a26c-998e0d1afc11",
                    "LayerId": "abbfc5f5-9acd-4d5d-9647-a84039a38196"
                }
            ]
        },
        {
            "id": "41a4a01b-4d55-4a4f-851e-88aefbb04c18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcdbc4ef-0166-441e-9c99-103837f183cd",
            "compositeImage": {
                "id": "9bc87932-3ba9-4fa9-9c26-9b5041cd163e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a4a01b-4d55-4a4f-851e-88aefbb04c18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09021f61-8df9-4c5a-877c-8b30c66f34d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a4a01b-4d55-4a4f-851e-88aefbb04c18",
                    "LayerId": "abbfc5f5-9acd-4d5d-9647-a84039a38196"
                }
            ]
        },
        {
            "id": "e55f054c-6154-40d5-8edb-f5c39a6bb9ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcdbc4ef-0166-441e-9c99-103837f183cd",
            "compositeImage": {
                "id": "d56f9a7b-7fdd-48f4-9830-974b88b79366",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e55f054c-6154-40d5-8edb-f5c39a6bb9ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "183b8f38-9f03-4b0a-b059-3db873055406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e55f054c-6154-40d5-8edb-f5c39a6bb9ed",
                    "LayerId": "abbfc5f5-9acd-4d5d-9647-a84039a38196"
                }
            ]
        },
        {
            "id": "0237fe7d-90f2-404d-b525-cd7791edb42c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcdbc4ef-0166-441e-9c99-103837f183cd",
            "compositeImage": {
                "id": "f0d69216-b9bd-4fff-aca3-d3c5cc599e1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0237fe7d-90f2-404d-b525-cd7791edb42c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d3baf55-a365-45d1-b701-58bd94fbe1c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0237fe7d-90f2-404d-b525-cd7791edb42c",
                    "LayerId": "abbfc5f5-9acd-4d5d-9647-a84039a38196"
                }
            ]
        },
        {
            "id": "bfe2e298-6914-400e-8b3d-4fe2a765b61e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcdbc4ef-0166-441e-9c99-103837f183cd",
            "compositeImage": {
                "id": "f567cfdb-56ae-43fc-81fd-aa58db931ab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfe2e298-6914-400e-8b3d-4fe2a765b61e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad537514-8f61-44b8-a3d2-6d3d3cea8321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfe2e298-6914-400e-8b3d-4fe2a765b61e",
                    "LayerId": "abbfc5f5-9acd-4d5d-9647-a84039a38196"
                }
            ]
        },
        {
            "id": "74840f98-2f5d-4fe8-8811-2e4e70ad0562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcdbc4ef-0166-441e-9c99-103837f183cd",
            "compositeImage": {
                "id": "e379ed3d-62bf-47de-ade1-12e8515bf044",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74840f98-2f5d-4fe8-8811-2e4e70ad0562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73040fab-af0d-4a7a-8b73-d0b093a3dadf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74840f98-2f5d-4fe8-8811-2e4e70ad0562",
                    "LayerId": "abbfc5f5-9acd-4d5d-9647-a84039a38196"
                }
            ]
        },
        {
            "id": "521fd52b-7f96-4528-9273-f7f8976ae574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcdbc4ef-0166-441e-9c99-103837f183cd",
            "compositeImage": {
                "id": "01cdacb9-e74e-4907-a001-338a595f2c60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "521fd52b-7f96-4528-9273-f7f8976ae574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fdf35d2-b3c9-4ae9-9e28-1719b9e47017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "521fd52b-7f96-4528-9273-f7f8976ae574",
                    "LayerId": "abbfc5f5-9acd-4d5d-9647-a84039a38196"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "abbfc5f5-9acd-4d5d-9647-a84039a38196",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcdbc4ef-0166-441e-9c99-103837f183cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}