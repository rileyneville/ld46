{
    "id": "5b762376-d939-4e96-9a6c-6a3e40caf0ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec6068f7-6dda-429f-8f1b-e66e81ffc5ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b762376-d939-4e96-9a6c-6a3e40caf0ba",
            "compositeImage": {
                "id": "a80db250-a3c4-49b9-8f87-a455d6a6d3e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec6068f7-6dda-429f-8f1b-e66e81ffc5ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccc114b3-bf6d-4be6-8b00-ec8861532145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec6068f7-6dda-429f-8f1b-e66e81ffc5ab",
                    "LayerId": "1a11dc8f-201a-4b36-85c9-b5fa6e9b42eb"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 16,
    "layers": [
        {
            "id": "1a11dc8f-201a-4b36-85c9-b5fa6e9b42eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b762376-d939-4e96-9a6c-6a3e40caf0ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}