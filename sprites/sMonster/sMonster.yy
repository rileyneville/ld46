{
    "id": "21ad179b-aebf-4425-bb79-5dfbbc688916",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMonster",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79bac139-2d0c-4613-b2ec-fa25114e668b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21ad179b-aebf-4425-bb79-5dfbbc688916",
            "compositeImage": {
                "id": "527e97ea-13c3-4927-97fc-6d50d9c8491d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79bac139-2d0c-4613-b2ec-fa25114e668b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10504790-c109-4197-b2dc-11effb07a211",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79bac139-2d0c-4613-b2ec-fa25114e668b",
                    "LayerId": "f47eae83-579f-4599-b853-dc7eddba3450"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f47eae83-579f-4599-b853-dc7eddba3450",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21ad179b-aebf-4425-bb79-5dfbbc688916",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}