{
    "id": "144c1a6a-a6f7-49fb-825f-e941a36b2619",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLargeHouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 37,
    "bbox_right": 91,
    "bbox_top": 45,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1b0dcc5-62e6-49af-9a1b-d47eb9defec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "144c1a6a-a6f7-49fb-825f-e941a36b2619",
            "compositeImage": {
                "id": "ec36e0f1-9f08-469c-9cc8-30dae9755846",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1b0dcc5-62e6-49af-9a1b-d47eb9defec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cbc03b4-18c6-465d-8e38-9e84defc17e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1b0dcc5-62e6-49af-9a1b-d47eb9defec6",
                    "LayerId": "aabb68d7-b8b0-4e7e-a4dc-8c5f68f6d8f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "aabb68d7-b8b0-4e7e-a4dc-8c5f68f6d8f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "144c1a6a-a6f7-49fb-825f-e941a36b2619",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}