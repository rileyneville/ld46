{
    "id": "dde527c8-4c2d-463f-a274-3c3f8f70029f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 22,
    "bbox_right": 45,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "383c93dc-709d-4fb1-888c-4b07482a033c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dde527c8-4c2d-463f-a274-3c3f8f70029f",
            "compositeImage": {
                "id": "6e306746-f017-4061-bae1-ae90826203e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "383c93dc-709d-4fb1-888c-4b07482a033c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a072ed89-9b3d-4079-8481-e38b9aeb281a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "383c93dc-709d-4fb1-888c-4b07482a033c",
                    "LayerId": "07f9ac2a-0723-48da-8e18-0f851bc0f3d8"
                },
                {
                    "id": "46c42b5e-0e94-49b6-b675-1cd7b5c234f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "383c93dc-709d-4fb1-888c-4b07482a033c",
                    "LayerId": "00f26884-ffda-41a4-aeef-849c1d48a60f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "00f26884-ffda-41a4-aeef-849c1d48a60f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dde527c8-4c2d-463f-a274-3c3f8f70029f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "07f9ac2a-0723-48da-8e18-0f851bc0f3d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dde527c8-4c2d-463f-a274-3c3f8f70029f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}