{
    "id": "fc2f275f-e1db-4ee2-b4fd-93a1e6daf0d0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt1",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\fnt1\\SansVampireGothi.ttf",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "SansVampireGothi",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a1b1f9a6-9d11-414a-a715-7740058b558a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9672bcd9-d26f-4dac-8630-66d059475faa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 16,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4661f4c4-d624-4d4e-a4a1-cc277958d4de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 11,
                "y": 74
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "300675f0-179a-4ea5-bf89-847339425ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "32e790fd-fee1-49db-8aa2-e69eb5a4a6d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 118,
                "y": 56
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "edc2b5b6-a923-4179-87db-2df3ecaa0c51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 108,
                "y": 56
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d5c5908b-dc31-43b2-b8db-a3b374ec7db2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 97,
                "y": 56
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "1f011e21-4045-48fa-833b-0a10d250b6ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 16,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 94,
                "y": 56
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "057601ba-e677-42d4-b8bc-b0af3ed628c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 89,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c5327147-def5-470d-9365-84483cafe161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 84,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a1192f6e-4acc-4730-a274-5d3237361344",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 21,
                "y": 74
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5aa346af-1bd2-44ae-a656-bfc4f84d28d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 75,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6476d705-af9d-4f00-9ddc-fc598433ad0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 64,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4f3f3537-f43e-4b52-9e35-2deec59e7fc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 57,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "28f7fb5c-a204-4b49-b9ba-30ed4cdb40c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 54,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "bd3da37d-0df5-41be-954c-6288dd81aa39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 48,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "649e2147-8924-4511-ab84-c75afa9690f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 41,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "01292e65-3b7e-4a1e-ac9e-e2e4d91353a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 35,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3913621c-7e15-4fd6-9dcd-479a096708e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 27,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7dba751f-a664-44ec-b5c5-713f5ffd2a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 20,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9d569669-f5e6-4a8c-91b8-43deb822ba0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 12,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fdc0afcd-190e-4034-99e2-abb612c45893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 68,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "810dc029-5179-4bbc-940e-4c2600e26acc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 26,
                "y": 74
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "08f4000b-210d-4dcb-8432-42bbbb13de84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 33,
                "y": 74
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "062dc1c1-a5ff-4807-a3a5-63d64156b47c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 41,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2d138815-74f9-4ca5-8e18-d6292fea658a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 87,
                "y": 92
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "68d3de7f-15b9-4ed3-b594-31388cfc1ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 84,
                "y": 92
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "3fba9166-5fdf-4e82-86eb-ea1228d77702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 80,
                "y": 92
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e536db6d-82b9-4928-a8d9-c828b8f1c243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 73,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7b4f4661-6593-4546-b851-c471cb5a1221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 65,
                "y": 92
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4afd4dac-164c-4c3c-8d8f-b06eef9dfee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 92
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3c2f81af-4d66-4a26-8d4e-397113c83e19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 92
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "af0b7e1c-4ed9-4ea8-805b-82ca4c7cb134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 39,
                "y": 92
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9130559f-3075-46f1-b6e5-7c7c3927b97f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 31,
                "y": 92
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f770eeae-3982-4fcc-9f62-a89df7cd5c51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 21,
                "y": 92
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fbe1a1fc-2ceb-4812-8109-cf2b83edb539",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 12,
                "y": 92
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b101dd4a-d5e7-492a-a8f8-5484023fedea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a4a62a96-35fc-421b-9ac9-1a2784c1f5ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 118,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "cc129b8f-6438-4940-852a-840814511e4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 110,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9e839c38-ba7a-4afc-8b54-27a989e16a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 100,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1b17acf6-da86-4c4d-ad4d-7da0807f306f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "280fc431-0e25-4c6a-8062-c2e6735459e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 86,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3cdfc60d-7ee6-41f3-85ad-006a38e18baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 78,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c946a896-6bb8-4978-8cec-c2608991aef5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 68,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1a79ef54-76f6-486d-b408-d97765896355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 59,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "af38f869-3979-47f4-aa63-d66c8af10873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 48,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a98b41f7-c1d1-4be7-87e3-8ab14b8fde85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "65c1919e-491c-4bc1-a60a-404d5d9fb81b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 109,
                "y": 38
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a7361c48-d7a8-4f9a-86b7-5b8b88425e5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 100,
                "y": 38
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "eee29920-2894-409d-b544-a7d2c40d64f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 55,
                "y": 20
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "072dfb39-5c0f-4f13-b6d2-cc32ce03ecc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 40,
                "y": 20
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f153ce87-7c13-42b0-8c13-82746b8c4fbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 31,
                "y": 20
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c25dbc21-aa3e-437c-895f-341de1824753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 21,
                "y": 20
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4990be5d-17cf-4255-8521-b2479eaa74c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 12,
                "y": 20
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "18e0ab24-43d2-4d14-9bd1-2ed350c23eb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f509638d-069e-4116-bfab-bbcf87c61899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1298be64-8ae4-4bec-a4c8-37a597e737dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "cd4567ea-43ba-412b-a621-5b6146f73889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0444cd0e-bfe5-4c0e-87a9-969486312236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "792cbcf4-308c-4bbc-83bb-dfad883c01a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 50,
                "y": 20
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fbc4fa43-d20c-4213-a524-007db20c77f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "843fbe02-3b59-4827-b992-476f9d825afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "79b8969e-8a45-4a37-91f0-cd89cbfe4955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4aec438b-c35a-46b5-8984-e249584a84c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b5e15d6c-5bfa-4774-9bdc-b24180bf242b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "273dd615-24bf-419c-aea8-3a25733825bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9a91c824-7188-4d63-9885-1cd7fd573369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2235e765-a9ea-458d-b7e8-fbe626690524",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "813569ac-0f02-4f95-af94-f243c07052db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "06f7d5f5-2bc8-4ad4-9885-55ea5f329bee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8907a3af-b465-417d-9d71-6ce45b88d3ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "54d4d51e-e9f5-4194-924a-de9646bea085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 20
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "aa840572-4da0-40dd-9036-68c0f874e4fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 16,
                "y": 38
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "184920ed-8485-4ac0-93bf-7723e28b310c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 72,
                "y": 20
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "580ef52a-0438-4ff4-a386-f631132c5fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 16,
                "offset": -2,
                "shift": 3,
                "w": 4,
                "x": 87,
                "y": 38
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8f6c8e1d-9b9c-4587-b7b8-a5b9bb06e9e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 80,
                "y": 38
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "49216af1-0875-49fd-b8e0-7b6771323d47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 74,
                "y": 38
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9a6bb2f5-351d-4fca-92a2-00dfe1234068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 66,
                "y": 38
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "905d1108-bf60-4039-9d1b-f1ed7b163ece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 60,
                "y": 38
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "51f5ddbb-5459-428a-a0b2-dcbf223190c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 53,
                "y": 38
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e6247dec-7a19-46de-9677-7c44fe41b6be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 16,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 45,
                "y": 38
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "cc816107-dad8-4d2e-ab7a-3c00b8aed564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 37,
                "y": 38
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ef890d9e-de07-4f6e-9f2c-05fca8ff0aec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 31,
                "y": 38
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "40cf9ad8-9464-47fe-a492-f92cae233100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 38
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8f80a8b0-ea92-4cc9-a9de-bfddc736e6be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 24,
                "y": 38
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "1dc86b63-3a4e-48fd-a0e0-3cd37d0c6aed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 8,
                "y": 38
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "70b5d6ce-4910-42b0-b4a2-f2b8a30eac18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "edcf26a5-1d83-41a8-9407-59d98056ce3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 115,
                "y": 20
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f475ac6e-8cab-41d3-b5fb-45113ac46c51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 108,
                "y": 20
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4d200c74-6091-40f4-a309-7961c85b2b48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 101,
                "y": 20
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "85acb6d5-b526-488b-8363-1be4c0899125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 94,
                "y": 20
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3c9d96cc-a56b-47f7-bb8e-ce2d448efc6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 16,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 87,
                "y": 20
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0442cb60-7a09-4bb7-9b42-b812f6e024d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 16,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 84,
                "y": 20
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ce3dc020-636b-48b7-9e35-f1bf35979097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 77,
                "y": 20
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2588426b-ae8f-4c09-a9c5-528fdcc2aaf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 94,
                "y": 92
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1b31edce-2688-4940-bf76-00390116c4c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 16,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 102,
                "y": 92
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}