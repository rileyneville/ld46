{
    "id": "fc2f275f-e1db-4ee2-b4fd-93a1e6daf0d0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt1",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\fnt1\\VampireGothic.ttf",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "VampireGothic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b9b98ab9-8bf3-46bf-a1d7-811f30a0096a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b76a2db0-a786-47e9-8916-ad06ecce8fa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 16,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "710638b5-e13b-4b63-879c-3011c79e95b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 11,
                "y": 74
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "92ef3a9a-ad25-4f40-a76d-38477a769811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "995a94ef-e2ad-4308-8243-bed2b79cf048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 118,
                "y": 56
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c1eae37d-8d2d-4713-9171-ae32b28ae994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 108,
                "y": 56
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "793b4f76-a841-47c5-9995-d2ad18244403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 97,
                "y": 56
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "510eda84-fb7f-4c47-a053-2fb874223a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 16,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 94,
                "y": 56
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "50676781-6dcf-440a-b3ad-0e8aeacff439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 89,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "945da3e0-fa58-4d37-a28f-a82e68ef8a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 84,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d774fe04-5428-406a-a6f1-4256a6d0b170",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 21,
                "y": 74
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6c157e9f-efb4-4ba1-8718-44c969120a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 75,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a723397a-0342-4a4f-b683-da57db41310e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 64,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "31c57c2d-ac46-43f7-a07c-563b548765cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 57,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3f45a92e-151e-4338-98df-f32c07c3de93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 54,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ac088052-361f-4b2d-b7c0-767afd033589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 48,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f8b0c6c6-e5da-49dc-8811-c38a49b762fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 41,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "24ee9388-9328-4e55-8586-47878a99762a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 35,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "079b8d3a-2254-4415-a69b-19b05d48ccfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 27,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e9ed1045-13f9-4611-9546-06e638a7727f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 20,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "edac58bf-9eda-492f-bb7b-fe6a93ce930e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 12,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8ccfe430-6d01-4d92-9759-721508035cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 68,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "796816a0-7df5-48de-82fe-426b0910eeb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 26,
                "y": 74
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "69904466-fbbb-43c9-9f30-b1b349361425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 33,
                "y": 74
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "25c7b0cc-e2b5-4255-a3f8-f27483506f09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 41,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "dc7f7bd1-6b01-4535-ad8e-c2a09607c107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 87,
                "y": 92
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ebc3d9d8-3bc2-4da1-9496-76912574c435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 84,
                "y": 92
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e1e438e1-595d-4f96-8330-36343102eaf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 80,
                "y": 92
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a84664d7-9293-405b-a171-d9d75bd182ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 73,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "97e292b9-4178-45d7-84bb-877fdf3a1b3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 65,
                "y": 92
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c912d966-8894-4beb-a505-13b9302839e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 92
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a11a225d-f063-418f-87dd-647492cbfd6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 92
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d42ffc47-f221-4978-8331-1728fbb89657",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 39,
                "y": 92
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8dfade09-7d81-4187-928d-28f775ad7976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 31,
                "y": 92
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "dffd77dc-82ea-4f51-b54e-ea854e3d0c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 21,
                "y": 92
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "933d1e89-ad52-4254-a040-6ba950ec376d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 12,
                "y": 92
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "6088be26-abd7-486e-a697-3c0c4d19ad33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4b26a49c-f0cb-43a2-b5c3-c447196d1f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 118,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "07e172ff-61fb-4233-99d5-e17515c69320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 110,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "36c5d154-a3df-4d91-a22d-1824db487178",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 100,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "72ea18ba-feb0-4f2a-b944-2720640bb783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6eff79d3-8099-448e-8818-1af7faca7723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 86,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "39b74000-4fc5-4227-82c2-45eec33dd018",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 78,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0ffe04c3-ea01-4031-ae19-4fcbbfcdc084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 68,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c9c55593-3706-4215-9526-7c6cd7ce316c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 59,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4b7502ad-33cc-467e-8fcc-162053741c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 48,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "612a2229-4159-4acc-8575-664e6182540e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9e89263d-1e43-4538-a735-2f2e4bd2a8e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 112,
                "y": 38
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b853216b-e8ef-461f-9500-51949b23aacc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 103,
                "y": 38
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a3d8c4c1-0bf7-4eff-ab4d-5906f69e3ec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 55,
                "y": 20
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9be65037-96b6-4ece-ade5-be30897ae7f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 40,
                "y": 20
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "43e87ed5-e187-47f2-ae41-a6b50ef925fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 31,
                "y": 20
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "004eecb2-4c2d-4f43-8828-d6e06ab1dc8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 21,
                "y": 20
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0519c54a-ec6c-41fd-888d-b5203c56ff95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 12,
                "y": 20
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4dd58a21-0d3a-444d-a4f4-28391a9c4e64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c5d08963-c977-4e35-b7ed-a174a4d9d31a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7b6b3931-fc8e-4b60-a59a-31417341ce97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bfe7ba46-eeca-45f3-89cc-7ee408551969",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ff030e54-c0da-4d20-8dea-bc1e15570027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1c3caf62-b113-4384-8ba7-6cd3c0ba7f0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 50,
                "y": 20
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "08d6520f-4f53-484f-b1eb-edd893fdbe80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "acb5c1dc-3617-4cb9-8ccd-85c911b7ecb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7d497b74-cd2b-47d4-bedd-7cc30d26e6a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fcd38523-7e50-4e64-b541-91960bb08731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5f621811-7c23-4635-9132-0fddaecdad0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "dd99f5d5-d2bc-44ac-ae9f-405ecc883369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "316837c5-2888-4946-8a23-a2b80567127b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6c7cb5b4-368d-4185-bfdf-8838f5d44dae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "18d7b05f-8559-49c7-b85d-fc3c388812a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e412ad35-4431-46e5-ad18-69b043538286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "aa07e60c-5af2-4703-995e-50254c191012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "93065dad-0010-4f26-973e-daf648070fec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 20
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8e97504b-5815-40f1-99ac-f2c70f2cbf80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 15,
                "y": 38
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "fa52dea9-e5b7-43ad-bc59-8cb3bcc3123e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 72,
                "y": 20
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bea7bd67-8c39-4123-9f3a-024ae1ba563e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 16,
                "offset": -2,
                "shift": 3,
                "w": 4,
                "x": 89,
                "y": 38
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7c850e9f-0334-47a5-bd04-977738c509d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 82,
                "y": 38
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "4bd371e5-a012-434a-b470-de48b753abd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 76,
                "y": 38
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c9ea47f1-f40f-4474-83ad-4a9c561fddd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 67,
                "y": 38
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "27b8132c-1954-4a74-9f40-51d7ab0e6bbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 60,
                "y": 38
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c115e162-c03e-4134-909b-b898c44f01bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 53,
                "y": 38
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "88a72d7f-8936-4d17-9e93-538817f07897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 16,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 45,
                "y": 38
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "91326470-ac6b-49fa-9064-63f86e44b637",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 37,
                "y": 38
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8bfe7be5-4943-407b-b000-1ccf020595e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 38
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "fb0b64e3-e1c2-4547-8cbc-ee4a188c891d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 95,
                "y": 38
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "7b301cc8-3805-4c7a-9ceb-cd3eff8c3dfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 38
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "10d9bcdd-a9d4-4c18-8b89-777799b9c043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 8,
                "y": 38
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9cf105aa-cd5f-4e87-9bd0-341897856dd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f591c5dc-31e9-4899-a9d4-0b6e66302444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 118,
                "y": 20
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7fa85e15-0714-4912-a7f1-eb5f36848a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 111,
                "y": 20
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "db4c46e4-fe71-4781-aaa8-6dc7851041b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 104,
                "y": 20
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a36aae80-531e-4901-955a-828e42f31ab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 95,
                "y": 20
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7ee9ad16-608c-4b8d-a1e0-272348d028bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 16,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 88,
                "y": 20
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6de26da4-0ff9-4d57-bd27-bf7ad180efbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 16,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 85,
                "y": 20
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "720dc227-ac13-4d2b-bc9c-3f646a0527a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 78,
                "y": 20
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "226deeb1-a8b6-4932-b8b3-0309b02fd608",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 94,
                "y": 92
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "8eea1296-f5f8-4f2e-98ee-e35f153dfc37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 16,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 102,
                "y": 92
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}