{
    "id": "afbb8a69-4eb6-41e3-a500-2afb19ae1f84",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRibs",
    "eventList": [
        {
            "id": "46f8f905-3226-48f7-b1c5-6b37caffb2c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "afbb8a69-4eb6-41e3-a500-2afb19ae1f84"
        },
        {
            "id": "d19ed8b3-8a10-47be-af5e-763bbfc0886e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "afbb8a69-4eb6-41e3-a500-2afb19ae1f84"
        },
        {
            "id": "3fa8987c-b85f-4e6b-9029-f6cfedd0bb61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "afbb8a69-4eb6-41e3-a500-2afb19ae1f84"
        },
        {
            "id": "96c9cfc4-0943-4bc3-9272-1d58b75281dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "afbb8a69-4eb6-41e3-a500-2afb19ae1f84"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ae9177f6-a196-4012-a1f5-a8b676c81991",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "10c80701-0878-4fdc-9d94-93187107c201",
    "visible": true
}