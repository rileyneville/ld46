{
    "id": "b373d771-6e57-4ac4-8604-d0059ee115b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSmallHouse",
    "eventList": [
        {
            "id": "77e15289-fab9-44fb-ae21-ae18a34e6d02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b373d771-6e57-4ac4-8604-d0059ee115b0"
        },
        {
            "id": "2d1d95df-7462-4e86-8256-075f032aecb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b373d771-6e57-4ac4-8604-d0059ee115b0"
        },
        {
            "id": "db7cf7ed-14c7-4e84-b657-aa58ad0ae4ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "b373d771-6e57-4ac4-8604-d0059ee115b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "72c2ae23-cb42-4468-a30a-f1260d337c57",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "99e9f5ef-15e3-4bf4-a3bf-6575b7ff1a23",
    "visible": true
}