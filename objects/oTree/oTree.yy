{
    "id": "32a8fb60-02c9-4ec5-95b3-d6cad574831f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTree",
    "eventList": [
        {
            "id": "6370c1c8-c914-49dd-b8be-f5911d84c921",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "32a8fb60-02c9-4ec5-95b3-d6cad574831f"
        },
        {
            "id": "d337d643-30cd-423e-963d-2ca9ce2a3768",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32a8fb60-02c9-4ec5-95b3-d6cad574831f"
        },
        {
            "id": "2f4d0af2-457f-4699-99f7-e8ca1fd4c1c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "32a8fb60-02c9-4ec5-95b3-d6cad574831f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ae9177f6-a196-4012-a1f5-a8b676c81991",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9df3da8-f1de-4101-b655-315d32336bef",
    "visible": true
}