{
    "id": "dd8eb661-1ca8-41cc-8f53-ae9a35555803",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSummoningCircle",
    "eventList": [
        {
            "id": "83eed133-289b-4fb9-934c-1daa2d22a47f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd8eb661-1ca8-41cc-8f53-ae9a35555803"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c93babb7-a111-45ce-8f2f-12a7b8e35747",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "87c0b4e6-40b9-4e92-8efd-b02b2af272d7",
    "visible": true
}