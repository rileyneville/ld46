{
    "id": "7771d553-b2ea-44b9-88a7-18b3b51a444d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBigTree",
    "eventList": [
        {
            "id": "c0087be5-d53d-4eb6-abc3-f9a654c440f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7771d553-b2ea-44b9-88a7-18b3b51a444d"
        },
        {
            "id": "b7b43af7-09a8-44c2-a293-dbcc1e83bd6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7771d553-b2ea-44b9-88a7-18b3b51a444d"
        },
        {
            "id": "799f78a5-4f1a-4f7e-bd5a-b8e4679d2e1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "7771d553-b2ea-44b9-88a7-18b3b51a444d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ae9177f6-a196-4012-a1f5-a8b676c81991",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e29bea4-04f7-47f1-aba7-c64ae6876eff",
    "visible": true
}