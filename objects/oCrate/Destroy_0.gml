/// @description 
part_type_direction(global.pt_splinters,oMonster.dir-60,oMonster.dir+60,0,0);
repeat(7) {
	part_particles_create(global.ps_ground,x+random_range(-radius,radius),y+random_range(-radius,radius),global.pt_splinters,1);
}
if (irandom(1)) {
	repeat(5) {
		var apple = instance_create_layer(x+random_range(-radius,radius),y+random_range(-radius,radius),"Ground",oApple);
		var ddir = point_direction(x,y,apple.x,apple.y);
		var spd = random_range(1,2);
		apple.hsp = lengthdir_x(spd,ddir);
		apple.vsp = lengthdir_y(spd,ddir);
	}
} else {
	repeat(5) {
		var apple = instance_create_layer(x+random_range(-radius,radius),y+random_range(-radius,radius),"Ground",choose(oRibs,oMorsel));
		var ddir = point_direction(x,y,apple.x,apple.y);
		var spd = random_range(1,2);
		apple.hsp = lengthdir_x(spd,ddir);
		apple.vsp = lengthdir_y(spd,ddir);
	}
}