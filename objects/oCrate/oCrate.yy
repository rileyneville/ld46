{
    "id": "a6692d8e-31bd-45f9-9c11-ff4f28f441b9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCrate",
    "eventList": [
        {
            "id": "a848a1ce-6b9a-4e97-b6eb-0fa5f01d9c3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a6692d8e-31bd-45f9-9c11-ff4f28f441b9"
        },
        {
            "id": "2b057564-bef5-47b4-b0a7-d03e91b3f084",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a6692d8e-31bd-45f9-9c11-ff4f28f441b9"
        },
        {
            "id": "12495ab4-5e6c-4d2f-90ca-b6a81f8ffbd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "a6692d8e-31bd-45f9-9c11-ff4f28f441b9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "72c2ae23-cb42-4468-a30a-f1260d337c57",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75eb64f4-0720-4bd0-8d59-73a6a8a7140e",
    "visible": true
}