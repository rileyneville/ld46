{
    "id": "f9a00279-347e-4b73-b1f4-7de67bcb4219",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMorsel",
    "eventList": [
        {
            "id": "93a48291-5470-497a-90c6-3e8ebab47040",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f9a00279-347e-4b73-b1f4-7de67bcb4219"
        },
        {
            "id": "b5d67c5a-fac3-43ab-9f80-5fec892b7c32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f9a00279-347e-4b73-b1f4-7de67bcb4219"
        },
        {
            "id": "d809c8e2-75ad-4fb0-be52-bcb721e59ed7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "f9a00279-347e-4b73-b1f4-7de67bcb4219"
        },
        {
            "id": "34f6331f-dde1-4172-8fba-af6661f6b193",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f9a00279-347e-4b73-b1f4-7de67bcb4219"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ae9177f6-a196-4012-a1f5-a8b676c81991",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "55719963-9dff-4169-b844-026055656360",
    "visible": true
}