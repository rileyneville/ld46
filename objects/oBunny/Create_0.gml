/// @description 

// Inherit the parent event
event_inherited();
radius = 3;
value = 100;
hsp = 0;
vsp = 0;
acc = 0.05;
run_spd = 1;
stand_sprite = sBunny;
run_sprite = sBunny_run;