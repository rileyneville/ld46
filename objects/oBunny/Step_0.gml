/// @description 
var spd = 0;
var run_dir = point_direction(0,0,hsp,vsp);
var light_dist = point_distance(x,y,global.light_x,global.light_y);
if (light_dist < 24) {
	run_dir = rlerp(run_dir,point_direction(global.light_x,global.light_y,x,y),1-light_dist/24);
	spd = run_spd;
}

if (distance_to_object(oMonster) < oMonster.radius*4) {
	run_dir = rlerp(run_dir,point_direction(oMonster.x,oMonster.y,x,y),1-distance_to_object(oMonster)/(oMonster.radius*4));
	spd = run_spd;
}

hsp = lerp(hsp,lengthdir_x(spd,run_dir),acc);
vsp = lerp(vsp,lengthdir_y(spd,run_dir),acc);
if (point_distance(0,0,hsp,vsp) > 0.1) {
	image_angle = rlerp(image_angle,point_direction(0,0,hsp,vsp),0.1);
}
if (point_distance(0,0,hsp,vsp) > 0.5) {
	sprite_index = run_sprite;
} else {
	sprite_index = stand_sprite;
}
move();