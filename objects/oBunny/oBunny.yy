{
    "id": "24eb0ec5-ae3a-4e16-8f32-cb9bb23b6c21",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBunny",
    "eventList": [
        {
            "id": "4efc4f82-3c00-4f85-b1f3-88ecf0221fc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "24eb0ec5-ae3a-4e16-8f32-cb9bb23b6c21"
        },
        {
            "id": "f1aabf03-cceb-4ba6-8f50-c8cb90371623",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "24eb0ec5-ae3a-4e16-8f32-cb9bb23b6c21"
        },
        {
            "id": "1bc174a9-c632-49de-bc35-b05c8c57f7e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "24eb0ec5-ae3a-4e16-8f32-cb9bb23b6c21"
        },
        {
            "id": "20205a84-deab-45e4-9d97-bc8f5a46e3cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "24eb0ec5-ae3a-4e16-8f32-cb9bb23b6c21"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ae9177f6-a196-4012-a1f5-a8b676c81991",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "11708ef0-5dce-4af5-8587-c0840ebf6fe0",
    "visible": true
}