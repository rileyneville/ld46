/// @description 
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
if (movement_tutorial > 0.1) {
	draw_text_color(guix(x),guiy(y+32),"WASD to move.",c_text,c_text,c_text,c_text,movement_tutorial);
} else {
	if (!summoning_complete && summon_circle_a == 0) {
		summoning_tutorial = lerp(summoning_tutorial,1,0.1);
	} else {
		summoning_tutorial = lerp(summoning_tutorial,0,0.1);
	}
	if (summoning_tutorial > 0.1) {
		var dx = guix(oSummoningCircle.x);
		var dy = guiy(oSummoningCircle.y);
		if (!point_in_rectangle(dx,dy,guix(x-32),guiy(y-32),guix(x+32),guiy(y+32)) || !place_meeting(x,y,oSummoningCircle)) {
			var ddir = point_direction(guix(x),guiy(y),dx,dy);
			draw_sprite_ext(sArrow,0,clamp(dx,guix(x-32),guix(x+32)),clamp(dy,guiy(y-32),guiy(y+32)),1,1,ddir,c_text,1);
		} else {
			draw_text_ext_color(guix(oSummoningCircle.x),guiy(oSummoningCircle.y+16),"Hold left mouse to summon it.",12,display_get_gui_width()-32,c_text,c_text,c_text,c_text,summoning_tutorial);
		}
	}
}
if (restart_tutorial > 0.1) {
	draw_text_ext_color(display_get_gui_width()/2,display_get_gui_height()-32,"Press R to start anew.",12,display_get_gui_width()-32,c_text,c_text,c_text,c_text,restart_tutorial);
}