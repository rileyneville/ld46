/// @description Devoured
devoured = true;
part_type_direction(global.pt_gore,oMonster.dir-60,oMonster.dir+60,0,0);
part_particles_create(global.ps_ground,x,y,global.pt_gore,12);

if (oMonster.ready_for_sacrifice) {
	notify("With the gift of your life, the whole world will be consumed. Rest well.");
} else {
	notify("You were devoured too soon.\nIt will never survive on its own.");
}