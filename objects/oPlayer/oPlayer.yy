{
    "id": "4acf4c5b-ca45-4f2b-86f5-672b1bacaabf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "e8486879-e106-4049-9dc5-635f4db33904",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4acf4c5b-ca45-4f2b-86f5-672b1bacaabf"
        },
        {
            "id": "e01bc0d1-504d-4596-8c0f-b1e66f163699",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4acf4c5b-ca45-4f2b-86f5-672b1bacaabf"
        },
        {
            "id": "58861076-0322-4857-89fd-2d4f73f41551",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4acf4c5b-ca45-4f2b-86f5-672b1bacaabf"
        },
        {
            "id": "f1d542b8-b5fb-4e18-a052-16a059459dfd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "4acf4c5b-ca45-4f2b-86f5-672b1bacaabf"
        },
        {
            "id": "d680755d-6109-479f-baf3-fc1bb5ae5ae3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4acf4c5b-ca45-4f2b-86f5-672b1bacaabf"
        },
        {
            "id": "4d20b235-fde1-40e8-b47b-a94a3d18bbd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "4acf4c5b-ca45-4f2b-86f5-672b1bacaabf"
        },
        {
            "id": "99c590c2-f25c-4e72-bce1-af12787b1b4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4acf4c5b-ca45-4f2b-86f5-672b1bacaabf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c93babb7-a111-45ce-8f2f-12a7b8e35747",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2e08845a-82ce-4188-b7d8-4b4814b38670",
    "visible": true
}