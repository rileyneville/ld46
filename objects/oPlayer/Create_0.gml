/// @description 
mvdir = 0;
spd = 0;
max_spd = 1;
acc = 0.1;
hsp = 0;
vsp = 0;
torch_dir = 0;
torch_x = x;
torch_y = y;
torch_lit = false;
radius = 5;
value = 400;

devoured = false;

pt_flame = part_type_create();
part_type_blend(pt_flame,true);
part_type_color1(pt_flame,c_orange);
part_type_life(pt_flame,15,30);
part_type_alpha3(pt_flame,0.5,1,0);
part_type_sprite(pt_flame,sPartFlame,false,false,true);
part_type_orientation(pt_flame,0,360,0,10,0);
part_type_speed(pt_flame,0,0.1,0.01,0.01);
part_type_direction(pt_flame,0,360,0,10);


pt_smoke = part_type_create();
part_type_blend(pt_smoke,false);
part_type_color3(pt_smoke,c_maroon,c_dkgray,c_black);
part_type_life(pt_smoke,30,60);
part_type_alpha3(pt_smoke,0,1,0);
part_type_sprite(pt_smoke,sPartSmoke,false,false,true);
part_type_orientation(pt_smoke,0,360,0,10,0);
part_type_speed(pt_smoke,0,0.1,-0.01,0.01);
part_type_direction(pt_smoke,0,360,0,10);

last_frame = image_index;

pt_footsteps = part_type_create();
part_type_sprite(pt_footsteps,sFootstep,false,false,true);
part_type_color1(pt_footsteps,c_black);
part_type_life(pt_footsteps,2000,3000);
part_type_alpha3(pt_footsteps,0.25,0.25,0);

ae_torch = audio_emitter_create();
audio_emitter_gain(ae_torch,0);
audio_emitter_falloff(ae_torch,320,740,1);
flame_sound = audio_play_sound_on(ae_torch,aSmallFlame,true,10);

summoning_complete = false;
summon_circle_a = 0;

movement_tutorial = 1;
summoning_tutorial = 0;
restart_tutorial = 0;