/// @description 
if (!audio_is_playing(flame_sound)) {
	flame_sound = audio_play_sound_on(ae_torch,aSmallFlame,true,10);
}
if (devoured) {
	torch_lit = 0;
	audio_emitter_gain(ae_torch,0);
	global.light_intensity = lerp(global.light_intensity,0.3,0.05);
	sprite_index = noone;
	if (!instance_exists(oNotification)) {
		restart_tutorial = lerp(restart_tutorial,1,0.05);
	}
	if (keyboard_check_pressed(ord("R"))) {
		audio_stop_all();
		game_restart();
	}
	return;
} 
/// ------------------------- Inputs -------------------------------
mvdir = point_direction(0,0,keyboard_check(ord("D"))-keyboard_check(ord("A")), keyboard_check(ord("S"))-keyboard_check(ord("W")));
spd = point_distance(0,0,keyboard_check(ord("D"))-keyboard_check(ord("A")), keyboard_check(ord("S"))-keyboard_check(ord("W")));
hsp = lerp(hsp,lengthdir_x(spd,mvdir),acc);
vsp = lerp(vsp,lengthdir_y(spd,mvdir),acc);
image_speed = spd;
if (spd != 0 || movement_tutorial < 1) {
	movement_tutorial = lerp (movement_tutorial,0,0.1);
}
move();

var mdir = point_direction(x,y,rmx(),rmy());
var mdist = min(point_distance(x,y,rmx(),rmy()),96);
torch_dir = rlerp(torch_dir,mdir,0.1);
image_angle = rlerp(image_angle,torch_dir,0.1);

if (summoning_complete) {
	
	if (!torch_lit && mouse_check_button(1)) {
		torch_lit = true;
	}
	if (keyboard_check_pressed(ord("E"))) {
		torch_lit = !torch_lit;
	}


	// ---------------------Torch Effects-------------------------------

	if (mouse_check_button(1) && torch_lit) {
		var tdir = point_direction(torch_x,torch_y,x+lengthdir_x(mdist,mdir),y+lengthdir_y(mdist,mdir));
		var tspd = min(point_distance(torch_x,torch_y,x+lengthdir_x(mdist,mdir),y+lengthdir_y(mdist,mdir)),4);
		torch_x += lengthdir_x(tspd,tdir);
		torch_y += lengthdir_y(tspd,tdir);
	} else {
		torch_x = lerp(torch_x,x+xoff(8,4,torch_dir),0.25);
		torch_y = lerp(torch_y,y+yoff(8,4,torch_dir),0.25);
	}
	//if (point_distance(global.light_x,global.light_y,torch_x,torch_y) > 4) {
		global.light_x = round(torch_x);
		global.light_y = round(torch_y);
	//}

	if (torch_lit) {
		global.light_intensity = lerp(global.light_intensity,1,0.2);
	
		part_particles_create(global.ps_air,torch_x,torch_y,pt_flame,1);
		if(!irandom(3)) {
			part_particles_create(global.ps_air,torch_x,torch_y,pt_smoke,1);
		}
	} else {
		global.light_intensity = lerp(global.light_intensity,0.1,0.05);
	}

	audio_emitter_position(ae_torch,torch_x,torch_y,0);
	audio_emitter_gain(ae_torch,torch_lit/2);
} else {
	if (mouse_check_button(1) && place_meeting(x,y,oSummoningCircle)) {
		summon_circle_a += 2;
	} else {
		summon_circle_a = max(summon_circle_a-4,0);
	}
	with (oSummoningCircle) {
		var min_light = 0.1;
		if (other.summon_circle_a != 0) {
			min_light = 0.5;
		}
		global.light_intensity = lerp(global.light_intensity,max(min_light,(other.summon_circle_a/420)*2),0.25);
		var r = -90;
		other.torch_x = other.x;
		other.torch_y = other.y+16;
		for (var i = 0; i < other.summon_circle_a; i++) {
			other.torch_x = x + lengthdir_x(radius,r-i);
			other.torch_y = y + lengthdir_y(radius,r-i);
			if(!irandom(abs(i-other.summon_circle_a))) {
				part_particles_create(global.ps_air,other.torch_x,other.torch_y,other.pt_flame,1);
			}
			if(!irandom(abs(i-other.summon_circle_a)*10)) {
				part_particles_create(global.ps_air,other.torch_x,other.torch_y,other.pt_smoke,1);
			}
		}
		if (other.summon_circle_a > 360) {
			other.summoning_complete = true;
			other.alarm[1] = 120;
		}
	}
	audio_emitter_position(ae_torch,torch_x,torch_y,0);
	global.light_x = lerp(global.light_x,(torch_x),0.05);
	global.light_y = lerp(global.light_y,(torch_y),0.05);
	audio_emitter_gain(ae_torch,summon_circle_a/180);
}

global.light_height = 32;

