/// @description 
if (devoured) {
	return;
}
draw_self_shadow(5);
draw_self();
draw_line_colour(x+xoff(1,6,torch_dir),y+yoff(1,6,torch_dir),x+xoff(8,4,torch_dir),y+yoff(8,4,torch_dir),$1E2E4C,$1E2E7C);
					