/// @description 

oCamera.target_x = x;
oCamera.target_y = y;
audio_listener_position(x,y,-8);

if (floor(image_index) != last_frame) {
	last_frame = floor(image_index);
	var foot_dir = round_to(point_direction(0,0,hsp,vsp),45);
	part_type_orientation(pt_footsteps,foot_dir,foot_dir,0,0,0);
	if (last_frame == 0) {
		part_particles_create(global.ps_ground,x+xoff(0,3),y+yoff(0,3),pt_footsteps,1);
	} else 	if (last_frame == 4) {
		part_particles_create(global.ps_ground,x+xoff(0,-3),y+yoff(0,-3),pt_footsteps,1);
	}
}