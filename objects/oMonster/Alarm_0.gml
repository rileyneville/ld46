/// @description Check if can eat player
if (!can_eat_player && (alarm[1] <= 0) && (radius > oPlayer.radius)) {
	alarm[1] = 360;
	notify("Its hunger grows.\nBe careful.");
}
if (!ready_for_sacrifice && radius > 30) {
	ready_for_sacrifice = true;
	notify("The time has come.\nIt is ready for your sacrifice.");
}
