/// @description 
if (!summoned) {
	return;
}


image_xscale = radius/64;
image_yscale = radius/64;
draw_self_shadow(radius/3);

// elbow kinematics -------------------------------

var A = point_distance(lshoulder_x,lshoulder_y,larm_x,larm_y)/2;
var H = radius*1.5;
var theta = cos(A/H);
var O = arcsin(theta)
if (is_undefined(O)) {
	O = 0;	
}
O*= H;


var ldir = point_direction(lshoulder_x,lshoulder_y,larm_x,larm_y);
var lx2 = lshoulder_x+xoff(A,-O,ldir);
var ly2 = lshoulder_y+yoff(A,-O,ldir);

var A = point_distance(rshoulder_x,rshoulder_y,rarm_x,rarm_y)/2;
var H = radius*1.5;
var theta = cos(A/H);
var O = arcsin(theta)
if (is_undefined(O)) {
	O = 0;	
}
O*= H;

var rdir = point_direction(rshoulder_x,rshoulder_y,rarm_x,rarm_y);
var rx2 = rshoulder_x+xoff(A,O,rdir);
var ry2 = rshoulder_y+yoff(A,O,rdir);


// Draw Arms
draw_set_colour(c_black);

var shadows_1 = get_shadow_offsets(lshoulder_x,lshoulder_y,radius/6);
var shadows_2 = get_shadow_offsets(lx2,ly2,larm_height+3);
var shadows_3 = get_shadow_offsets(larm_x,larm_y,larm_height);
draw_line_width(shadows_1[0],shadows_1[1],shadows_2[0],shadows_2[1],2);
draw_line_width(shadows_2[0],shadows_2[1],shadows_3[0],shadows_3[1],2);


var shadows_1 = get_shadow_offsets(rshoulder_x,rshoulder_y,radius/6);
var shadows_2 = get_shadow_offsets(rx2,ry2,rarm_height+3);
var shadows_3 = get_shadow_offsets(rarm_x,rarm_y,rarm_height);
draw_line_width(shadows_1[0],shadows_1[1],shadows_2[0],shadows_2[1],2);
draw_line_width(shadows_2[0],shadows_2[1],shadows_3[0],shadows_3[1],2);

draw_set_colour(c_white);

draw_line_width_color(x,y,lshoulder_x,lshoulder_y,2,image_blend,image_blend);
draw_line_width_color(lshoulder_x,lshoulder_y,lx2,ly2,2,image_blend,image_blend);
draw_line_width_color(lx2,ly2,larm_x,larm_y,2,image_blend,c_maroon);

draw_line_width_color(x,y,rshoulder_x,rshoulder_y,2,image_blend,image_blend);
draw_line_width_color(rshoulder_x,rshoulder_y,rx2,ry2,2,image_blend,image_blend);
draw_line_width_color(rx2,ry2,rarm_x,rarm_y,2,image_blend,c_maroon);



// body -----------------------------------------
draw_primitive_begin(pr_trianglestrip);
draw_vertex_color(x+xoff(0,radius),y+yoff(0,radius),c_black,1);
draw_vertex_color(x+xoff(0,-radius),y+yoff(0,-radius),c_black,1);

for (var i = 0; i < ds_list_size(tail);i++) {
	var segment = tail[| i];
	var rad = segment[Tail.Radius]*segment[Tail.Scale];
	var scale = rad/64;
	
	var dx = segment[Tail.X];
	var dy = segment[Tail.Y];
	var shadows = get_shadow_offsets(dx,dy,segment[Tail.Radius]/3);
	dx = shadows[0];
	dy = shadows[1];
	draw_vertex_color(dx+xoff(0,rad,segment[Tail.Dir]),dy+yoff(0,rad,segment[Tail.Dir]),c_black,1);
	draw_vertex_color(dx+xoff(0,-rad,segment[Tail.Dir]),dy+yoff(0,-rad,segment[Tail.Dir]),c_black,1);
}
draw_vertex_color(segment[Tail.X]+xoff(-rad*2,0,segment[Tail.Dir]),segment[Tail.Y]+yoff(-rad*2,0,segment[Tail.Dir]),c_black,1);
draw_primitive_end();

draw_primitive_begin(pr_trianglestrip);
draw_vertex_color(x+xoff(0,radius),y+yoff(0,radius),image_blend,1);
draw_vertex_color(x+xoff(0,-radius),y+yoff(0,-radius),image_blend,1);
for (var i = 0; i < ds_list_size(tail);i++) {
	var segment = tail[| i];
	var rad = segment[Tail.Radius]*segment[Tail.Scale];
	var scale = rad/64;
	draw_shadow(sMonsterPart,1,segment[Tail.X],segment[Tail.Y],scale,scale,segment[Tail.Dir],segment[Tail.Radius]/2);
	draw_sprite_ext(sMonsterPart,1,segment[Tail.X],segment[Tail.Y],scale,scale,segment[Tail.Dir],image_blend,1);
	
	draw_vertex_color(segment[Tail.X]+xoff(0,rad,segment[Tail.Dir]),segment[Tail.Y]+yoff(0,rad,segment[Tail.Dir]),image_blend,1);
	draw_vertex_color(segment[Tail.X]+xoff(0,-rad,segment[Tail.Dir]),segment[Tail.Y]+yoff(0,-rad,segment[Tail.Dir]),image_blend,1);
	draw_vertex_color(segment[Tail.X]+xoff(-rad/2,rad*0.866,segment[Tail.Dir]),segment[Tail.Y]+yoff(-rad/2,rad*0.866,segment[Tail.Dir]),image_blend,1);
	draw_vertex_color(segment[Tail.X]+xoff(-rad/2,-rad*0.866,segment[Tail.Dir]),segment[Tail.Y]+yoff(-rad/2,-rad*0.866,segment[Tail.Dir]),image_blend,1);
}
draw_vertex_color(segment[Tail.X]+xoff(-rad*2,0,segment[Tail.Dir]),segment[Tail.Y]+yoff(-rad*2,0,segment[Tail.Dir]),image_blend,1);
draw_primitive_end()
draw_self();

/*
if (arm_up == 0) {
	draw_circle_colour(tlarm_x,tlarm_y,4,c_green,c_green,true);
	draw_circle(trarm_x,trarm_y,4,true);
} else {
	draw_circle(tlarm_x,tlarm_y,4,true);
	draw_circle_colour(trarm_x,trarm_y,4,c_green,c_green,true);
}