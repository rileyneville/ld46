{
    "id": "5b2df235-67e8-4ca0-b9d0-60da85a6782b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMonster",
    "eventList": [
        {
            "id": "76132a25-9d7b-411d-9f12-4895c5ee2273",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5b2df235-67e8-4ca0-b9d0-60da85a6782b"
        },
        {
            "id": "4165da6b-4654-4a35-ad55-c6f65075b95c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5b2df235-67e8-4ca0-b9d0-60da85a6782b"
        },
        {
            "id": "ecf33ed0-c106-4f05-b6be-b04f04b83f79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5b2df235-67e8-4ca0-b9d0-60da85a6782b"
        },
        {
            "id": "e0e1fdf6-6f93-4d3f-a0dd-34e2c7a621bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "5b2df235-67e8-4ca0-b9d0-60da85a6782b"
        },
        {
            "id": "d3362cf0-9a23-4074-bab2-3b21f00bfe8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5b2df235-67e8-4ca0-b9d0-60da85a6782b"
        },
        {
            "id": "146688cb-7c52-4748-ada5-9e91ab186f88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "5b2df235-67e8-4ca0-b9d0-60da85a6782b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c93babb7-a111-45ce-8f2f-12a7b8e35747",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5da087c7-b88f-4d4b-ae4d-ed67c588abe1",
    "visible": true
}