/// @description 
if (!audio_is_playing(purr_sound)) {
purr_sound = audio_play_sound_on(ae,aCatPurrLoop_boosted,true,10);
}
if (!audio_is_playing(vocal_sounds)) {
	vocal_sounds = audio_play_sound_on(ae,aSpookyMonsterVocals,true,10);
	//audio_sound_gain(vocal_sounds,0.5,0);
}
if (!audio_is_playing(sliding_sound)) {
	sliding_sound = audio_play_sound_on(ae2,aSlimyMouthNoises,true,10);
}

if (!summoned) {
	return;
	
} else {
	gain = lerp(gain,min(mass/2000,2),0.01);
	pitch = clamp(map_range(mass,200,2000,1.25,0.25),0.5,1.25);
	audio_emitter_gain(ae,gain);
	audio_emitter_gain(ae2,gain);
	audio_emitter_pitch(ae,pitch);
	audio_emitter_position(ae,x,y,0);
	audio_emitter_position(ae2,x,y,0);
}
hungry = false;
if (instance_exists(target_food)) {
	if (target_food.radius > radius) {
		target_food = noone;
	} else if (target_food.object_index == oPlayer) {
		target_x = target_food.x;
		target_y = target_food.y;
		if (oPlayer.torch_lit) {
			target_food = noone;
		} else {
			hungry = true;
		}
	} else {
		target_x = target_food.x;
		target_y = target_food.y;
	} 
} 
if (!instance_exists(target_food)){
	if (oPlayer.devoured) {
		target_x = x+lengthdir_x(64,dir);
		target_y = y+lengthdir_y(64,dir);
	} else if (oPlayer.torch_lit) {
		ds_list_clear(target_list);
		collision_circle_list(oPlayer.torch_x,oPlayer.torch_y,32,oPickup,true,true,target_list,true);
		if (can_eat_player) {
			ds_list_add(target_list,oPlayer);
		}
		
		for (var i = 0; i < ds_list_size(target_list); i++) {
			var t = target_list[|i];
			if (t.radius < radius) {
				target_food = t;
				depth = min(target_food.depth-1,depth);
				if (target_food.object_index == oPlayer) {
					hungry = true;
				}
				break;
			}
		}
		if (!instance_exists(target_food)) {
			tdir = point_distance(oPlayer.torch_x,oPlayer.torch_y,x,y);
			target_x = oPlayer.torch_x+lengthdir_x(16,tdir);
			target_y = oPlayer.torch_y+lengthdir_y(16,tdir);
		}
	}
	
}

if (can_eat_player) {
	depth = min(depth,oPlayer.depth-1);
} else {
	depth = max(depth,oPlayer.depth+1);
}
with (target_food) {
	depth = max(depth,other.depth+1);	
}
tdist = point_distance(x,y,target_x,target_y);
tdir = point_direction(x,y,target_x,target_y);


tlarm_x = x+xoff(radius*3,-radius*2,tdir);
tlarm_y = y+yoff(radius*3,-radius*2,tdir);
						
trarm_x = x+xoff(radius*3,radius*2,tdir);
trarm_y = y+yoff(radius*3,radius*2,tdir);


lshoulder_x = x+xoff(-radius/2,-radius*0.7);
lshoulder_y = y+yoff(-radius/2,-radius*0.7);
rshoulder_x = x+xoff(-radius/2,radius*0.7);
rshoulder_y = y+yoff(-radius/2,radius*0.7);

armspd = clamp(map_range(mass,200,2000,0.5,0.15),0.15,0.5);
if (instance_exists(target_food)) {
	if (distance_to_object(target_food) < radius) {
		if ((point_distance(larm_x,larm_y,target_food.x,target_food.y) < target_food.radius) ||
		(point_distance(rarm_x,rarm_y,target_food.x,target_food.y) < target_food.radius)
		|| position_meeting(larm_x,larm_y,target_food)|| position_meeting(rarm_x,rarm_y,target_food)) {
			target_food.x = lerp(target_food.x,x,0.2);
			target_food.y = lerp(target_food.y,y,0.2);

			armspd *= 2;
		}
		
		tlarm_x = target_food.x;
		tlarm_y = target_food.y;
		
		trarm_x = target_food.x;
		trarm_y = target_food.y;
		
		move_body = false;
	}
	if (!point_in_rectangle(target_food.x,target_food.y,oCamera.left,oCamera.top,oCamera.right,oCamera.bottom)) {
		if (collision_circle(x,y,radius,target_food,false,false) == noone) {
			target_food = noone;
		}
	}
}

if (move_body) {
	dir = rlerp(dir,tdir,acc);
	if (tdist > 32 || instance_exists(target_food)) {
		spd = lerp(spd,min(max_spd,tdist),acc);
	} else {
		spd = lerp(spd,0,acc);
	}
	
	if ((point_distance(larm_x,larm_y,lshoulder_x,lshoulder_y) > radius*4) || (point_distance(x,y,larm_x,larm_y) < radius)) {
		arm_up = 0;
		move_body = false;
	}
	if ((point_distance(rarm_x,rarm_y,rshoulder_x,rshoulder_y) > radius*4) || (point_distance(x,y,rarm_x,rarm_y) < radius)) {
		arm_up = 1;
		move_body = false;
	}
	
	var arm_dist = point_distance(x,y,(larm_x+rarm_x)/2,(larm_y+rarm_y)/2);
	if ((arm_dist < radius/2) || (arm_dist > radius*3)) {
		move_body = false;
	}
} else {
	dir = rlerp(dir,tdir,acc/5);
}

if (!move_body){
	spd = lerp(spd,0,acc);
	if (arm_up == 0) {
		
		larm_x = lerp(larm_x,tlarm_x,armspd);
		larm_y = lerp(larm_y,tlarm_y,armspd);
			
		if ((point_distance(larm_x,larm_y,tlarm_x,tlarm_y) > radius/4)) {
			larm_height = lerp(larm_height,radius,0.1);
		} else {
			var airborne = larm_height > 1;
			larm_height = lerp(larm_height,0,0.2);
			if (larm_height < 1) {
				larm_height = 0;
				arm_up = 1;
				move_body = true;
				if (airborne) {
					var fs = audio_play_sound_at(array_random(thuds),larm_x,larm_y,0,radius,radius*4,1,false,10);
					audio_sound_pitch(fs,max(pitch/2,0.35));
				}
			}
		}
	} else {
		rarm_x = lerp(rarm_x,trarm_x,armspd);
		rarm_y = lerp(rarm_y,trarm_y,armspd);
		if ((point_distance(rarm_x,rarm_y,trarm_x,trarm_y) > radius/4)) {
			rarm_height = lerp(rarm_height,radius,0.1);
		} else {
			var airborne = rarm_height > 1;
			rarm_height = lerp(rarm_height,0,0.2);
			if (rarm_height < 1) {
				rarm_height = 0;
				arm_up = 0;
				move_body = true;
				if (airborne) {
					var fs = audio_play_sound_at(array_random(thuds),rarm_x,rarm_y,0,radius,radius*4,1,false,10);
					audio_sound_pitch(fs,max(pitch/2,0.35));
				}
			}
		}
	}
}

audio_sound_gain(sliding_sound,2*spd/max_spd,10);

x += lengthdir_x(spd,dir);
y += lengthdir_y(spd,dir);
image_angle = dir;
trail_counter -= spd;
if (trail_counter < 0) {
	var r = max(radius,4);
	trail_counter = r*random_range(0.5,1.5);
	part_type_size(pt_trail,r/16,r/16,0,0);
	part_type_orientation(pt_trail,dir,dir,0,0,0);
	part_particles_create(global.ps_ground,x,y,pt_trail,1);
}

ds_list_clear(target_list);
collision_circle_list(x,y,radius,oImpassable,false,false,target_list,true);
for (var i = 0; i < ds_list_size(target_list); i++) {
	var wall = target_list[|i];
	if (!rectangle_in_rectangle(wall.bbox_left,wall.bbox_top,wall.bbox_right,wall.bbox_bottom,oCamera.left,oCamera.top,oCamera.right,oCamera.bottom)) {
		continue;	
	}
	if (wall != target_food) {
		while (collision_circle(x,y,radius,wall,false,false) != noone) {
			var pdir = point_direction(wall.x,wall.y,x,y);
			x += lengthdir_x(1,pdir);
			y += lengthdir_y(1,pdir);
			if (instance_exists(target_food)) {
				if (collision_line(x,y,target_food.x,target_food.y,wall,false,false) != noone) {
					target_food = noone;
				}
			}
		}
	}
}

if (!hungry) {
	
	while (collision_circle(x,y,radius,oPlayer,false,false) != noone) {
		var pdir = point_direction(oPlayer.x,oPlayer.y,x,y);
		x += lengthdir_x(1,pdir);
		y += lengthdir_y(1,pdir);
		oPlayer.hsp = lerp(oPlayer.hsp,0,0.1);
		oPlayer.vsp = lerp(oPlayer.vsp,0,0.1);
	}
}
var destroy_food = false;
with(collision_circle(x,y,radius,target_food,false,false)) {
	if (radius < other.radius) {
		var c = min(value,max(1,value*0.1));
		other.mass += c*other.food_scalar;
		other.total_mass += c*other.food_scalar;
		value -= c;
		other.move_body = false;
		if (value <= 0) {
			destroy_food = true;
		}
		if (object_index == oPlayer) {
			torch_lit = false;	
		} else {
			image_xscale = lerp(image_xscale,0.25,0.1);
			image_yscale = image_xscale;
		}
	}
}
if (destroy_food) {
	if (target_food.object_index == oPlayer) {
		oPlayer.alarm[0] = 1;	
	} else {
		instance_destroy(target_food);
	}
	target_food = noone;
	alarm[0] = 60;
}

var first_segment = tail[|0];
if (first_segment[Tail.Mass] < mass *1.25) {
	var c = mass*0.01;
	first_segment[Tail.Mass] += c;
	tail[|0] = first_segment;
	mass -= c;
}
radius = sqrt(mass)/pi;

	
var lx = x;
var ly = y;
var ld = dir;
var lr = 8;
var lm = mass;
var last_segment = -1;
for (var i = 0; i < ds_list_size(tail); i++;) {
	var segment = tail[| i];
	
	if ((i > 0) && (segment[Tail.Mass] < lm*0.5)) {
		var c = lm*0.01;
		last_segment[Tail.Mass] -= c;
		tail[|i-1] = last_segment;
		segment[Tail.Mass] += c;
	}
	if ((i == ds_list_size(tail)-1) && (segment[Tail.Mass] > 150)) {
		tail[| i+1] = [segment[Tail.X],segment[Tail.Y],0,1,1,1];
		segment[Tail.Mass] -= 1;
	// -----------------	
	}
	segment[Tail.Radius] = sqrt(segment[Tail.Mass])/pi;
	
	var dist = point_distance(segment[Tail.X],segment[Tail.Y],lx,ly);
	
	var ddir = rclamp(point_direction(segment[Tail.X],segment[Tail.Y],lx,ly),ld-80,ld+80);
	var tx = lx-lengthdir_x(lr,ddir);
	var ty = ly-lengthdir_y(lr,ddir);
	
	with (collision_circle(x,y,radius,oPickup,false,false)) {
		if (!static && id != other.target_food) {
			var pdir = point_direction(segment[Tail.X],segment[Tail.Y],x,y);
			x += lengthdir_x(1,pdir);
			y += lengthdir_y(1,pdir);
		}
	}
	if (!hungry) {
		while (collision_circle(segment[Tail.X],segment[Tail.Y],segment[Tail.Radius]*segment[Tail.Scale],oPlayer,false,false) != noone) {
			var pdir = point_direction(oPlayer.x,oPlayer.y,segment[Tail.X],segment[Tail.Y]);
			segment[Tail.X] += lengthdir_x(1,pdir);
			segment[Tail.Y] += lengthdir_y(1,pdir);
			oPlayer.hsp = lerp(oPlayer.hsp,0,0.1);
			oPlayer.vsp = lerp(oPlayer.vsp,0,0.1);
		}
	}
	segment[Tail.Dir] = rclamp(rlerp(segment[Tail.Dir],point_direction(segment[Tail.X],segment[Tail.Y],tx,ty),0.1),ld-90,ld+90);
	segment[Tail.X] = lerp(segment[Tail.X],tx,0.1);
	segment[Tail.Y] = lerp(segment[Tail.Y],ty,0.1);
	var dist = point_distance(segment[Tail.X],segment[Tail.Y],tx,ty);
	
	if (dist < lr/2) {
		segment[Tail.Scale] = 1+(lr-dist)/(4*lr);
	} else {
		segment[Tail.Scale] = 1-(dist-lr)/(4*lr);
	}
	lx = segment[Tail.X];
	ly = segment[Tail.Y];
	ld = segment[Tail.Dir];
	lr = segment[Tail.Radius];
	lm = segment[Tail.Mass];
	last_segment = segment;
	tail[|i] = segment;
}

if (!oPlayer.devoured) {
	x = clamp(x,oCamera.left-radius,oCamera.right+radius);
	y= clamp(y,oCamera.top-radius,oCamera.bottom+radius);
}

