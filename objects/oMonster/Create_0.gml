/// @description
food_scalar = 0.75;
target_food = noone;
target_x = x;
target_y = y;
spd = 0;
dir = 0;
max_spd = 2;
acc = 0.1;
armspd = 0.2;
image_blend = $766C7C;
radius = 6;
total_mass = 100;
mass = total_mass-1;
can_eat_player = false;
enum Tail {
	X,
	Y,
	Dir,
	Mass,
	Radius,
	Scale
}
tail = array_to_list([[x,y,0,1,1,1]]);


pt_trail = part_type_create();
part_type_sprite(pt_trail,sSmeer,false,false,true);
part_type_color3(pt_trail,c_blood,c_maroon,c_maroon);
part_type_life(pt_trail,2000,3000);
part_type_alpha3(pt_trail,1,1,0);
trail_counter = 0;
hungry = false;

target_list = ds_list_create();

lshoulder_x = x;
lshoulder_y = y;
larm_x = x;
larm_y = y;
tlarm_x = x;
tlarm_y = y;
larm_height = 0;


rshoulder_x = x;
rshoulder_y = y;
rarm_x = x;
rarm_y = y;
trarm_x = x;
trarm_y = y;
rarm_height = 0;

arm_up = 0;
move_body = true;

summoned = false;

ae = audio_emitter_create();
ae2 = audio_emitter_create();
audio_emitter_gain(ae,0);
audio_emitter_falloff(ae,98,320,1);
audio_emitter_falloff(ae2,98,320,1);
purr_sound = audio_play_sound_on(ae,aCatPurrLoop_boosted,true,10);
vocal_sounds = audio_play_sound_on(ae,aSpookyMonsterVocals,true,10);
//audio_sound_gain(vocal_sounds,0.5,0);

sliding_sound = audio_play_sound_on(ae2,aSlimyMouthNoises,true,10);
audio_sound_gain(sliding_sound,0,0);
gain = 0;
pitch = 2;

thuds = [aThud_01,aThud_02,aThud_03,aThud_04,aThud_05,aThud_06,aThud_07];
ready_for_sacrifice = false;