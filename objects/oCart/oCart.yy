{
    "id": "bb4e171a-b39b-45be-b096-8bd715a53bcc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCart",
    "eventList": [
        {
            "id": "a662b5af-a756-4f93-879e-39f803165ad9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bb4e171a-b39b-45be-b096-8bd715a53bcc"
        },
        {
            "id": "9ef019a9-9dd4-48ab-bdcb-b9775d75cbc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bb4e171a-b39b-45be-b096-8bd715a53bcc"
        },
        {
            "id": "b70aaa74-9239-4f99-affa-f65a5449e1d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "bb4e171a-b39b-45be-b096-8bd715a53bcc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "72c2ae23-cb42-4468-a30a-f1260d337c57",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dde527c8-4c2d-463f-a274-3c3f8f70029f",
    "visible": true
}