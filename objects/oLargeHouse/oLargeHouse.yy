{
    "id": "d1ae8a52-6682-4b14-ab24-c8cc2a52de26",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLargeHouse",
    "eventList": [
        {
            "id": "c95124ca-2e60-4637-a834-48702e52f00d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d1ae8a52-6682-4b14-ab24-c8cc2a52de26"
        },
        {
            "id": "eb098126-12fd-42d6-98ca-084d7d6e4dd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d1ae8a52-6682-4b14-ab24-c8cc2a52de26"
        },
        {
            "id": "84fccc0f-d81a-4de6-8cd9-67a1e80cec19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d1ae8a52-6682-4b14-ab24-c8cc2a52de26"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "72c2ae23-cb42-4468-a30a-f1260d337c57",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "144c1a6a-a6f7-49fb-825f-e941a36b2619",
    "visible": true
}