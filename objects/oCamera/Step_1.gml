
if (!surface_exists(fg_surface)) {
	fg_surface = surface_create(base_camera_w,base_camera_h);
	view_set_surface_id(0,fg_surface);
}

if (browser_width != window_get_width() || browser_height != window_get_height()) {
	var margin = 0;
	if (window_get_width() != browser_width-margin*2 || window_get_height() != browser_height-margin*2) {
		window_set_size(browser_width-margin*2,browser_height-margin*2);
		window_set_position(margin,margin);
		reset_view = true;
	}
}

if ((window_get_height() != window_h || window_get_width() != window_w
		|| display_get_gui_height() != gui_h || display_get_gui_width() != gui_w || reset_view
		|| global.fullscreen != window_get_fullscreen()) 
		&& window_get_height() > 0 && window_get_width() > 0) {
	reset_view = false;
	
	global.fullscreen = window_get_fullscreen();
	var new_window_w = window_get_width();
	var new_window_h = window_get_height();
	if (new_window_h > 0 && new_window_w > 0) {
		window_w = new_window_w;
		window_h = new_window_h;
		window_ratio = window_w/window_h;
		
		base_camera_h = new_window_h*2;
		game_scale = 0.5;
		repeat(10) {
			if (game_scale < 1) {
				game_scale += 0.25;
			} else {
				game_scale++;
			}
			if ((abs(base_camera_h-target_vres) > abs((new_window_h/game_scale)-target_vres)) || (base_camera_h > max_vres)) {
				base_camera_h = (new_window_h/game_scale);
			} else {
				break;
			}
		}
		base_camera_h = round_to(base_camera_h,2);
		base_camera_w = round_to(base_camera_h*window_ratio,2);
		
		gui_h = base_camera_h;
		gui_w = base_camera_w;
		display_set_gui_size(base_camera_w,base_camera_h);
		surface_resize(application_surface,window_w,window_h);
		game_scale = new_window_h/base_camera_h;
		base_camera_h += 1;
		base_camera_w += 1;
		
		show_debug_message("resolution: "+string(base_camera_w)+"x"+string(base_camera_h)+", scale: "+string(game_scale));
			
		
		view_set_hport(0,base_camera_h);
		view_set_wport(0,base_camera_w);
		view_set_camera(0,camera);
		view_set_surface_id(0,fg_surface);
		
		
		
		surface_resize(fg_surface,base_camera_w,base_camera_h);
		
		

		camera_w = base_camera_w/zoom;
		camera_h = base_camera_h/zoom;
		
		
	}
	
}


image_xscale = camera_w/32;
image_yscale = camera_h/32;
	
if (instance_exists(oPlayer)) {
	margin = 64;
	target_x = clamp(target_x,oPlayer.x-camera_w/2+margin,oPlayer.x+camera_w/2-margin);
	target_y = clamp(target_y,oPlayer.y-camera_h/2+margin,oPlayer.y+camera_h/2-margin);
}

if (!locked) {
		
	if (instance_exists(oPlayer)) {
		margin = 64;
		x = clamp(x,oPlayer.x-camera_w/2+margin,oPlayer.x+camera_w/2-margin);
		y = clamp(y,oPlayer.y-camera_h/2+margin,oPlayer.y+camera_h/2-margin);
	}
		
	if (abs(x-target_x) > deadzone || abs(last_moved_x) >= 0.5) {
		x = lerp(x,target_x,camera_acc);
	}
		
	if (abs(y-target_y) > deadzone || abs(last_moved_y) >= 0.5) {
		y = lerp(y,target_y,camera_acc);
	}
}
			
	
		
target_zoom = clamp(target_zoom,0.25,10);
last_zoom = zoom;
zoom = lerp(zoom,target_zoom,zoom_acc);
base_camera_h = view_hport[0];

camera_w = base_camera_w/zoom;
camera_h = base_camera_h/zoom;
	
	
	
if (lock_to_room) {
	x = clamp(x,global.room_min_x+camera_w/2,global.room_max_x+-camera_w/2);
	y = clamp(y,global.room_min_y+camera_h/2,global.room_max_y+-camera_h/2);
}

var view_x = (x-camera_w/2);
var view_y = (y-camera_h/2);
	
camera_set_view_size(camera,camera_w,camera_h);
camera_set_view_pos(camera,view_x,view_y);


left = camera_get_view_x(camera);
right = left + camera_w;
top = camera_get_view_y(camera);
bottom = top + camera_h;


last_moved_x = x-xprevious;
last_moved_y = y-yprevious;

