{
    "id": "4e702ca3-3139-434c-a15e-8cd46fb0f7b6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCamera",
    "eventList": [
        {
            "id": "d1dd2576-0ff7-4c12-8fcf-380203a360b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4e702ca3-3139-434c-a15e-8cd46fb0f7b6"
        },
        {
            "id": "99a71613-e49e-4319-8d1e-891f3b7b4eda",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "4e702ca3-3139-434c-a15e-8cd46fb0f7b6"
        },
        {
            "id": "7a1d6026-043e-4d7e-be39-30428fddf4f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "4e702ca3-3139-434c-a15e-8cd46fb0f7b6"
        },
        {
            "id": "0d2e907b-09dc-41e3-9805-4bd5406e322a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "4e702ca3-3139-434c-a15e-8cd46fb0f7b6"
        },
        {
            "id": "d5a14eef-d86b-4cc9-a395-cf1438405e6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 76,
            "eventtype": 8,
            "m_owner": "4e702ca3-3139-434c-a15e-8cd46fb0f7b6"
        },
        {
            "id": "9d532a5a-4b26-4799-991d-e99733820952",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4e702ca3-3139-434c-a15e-8cd46fb0f7b6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c93babb7-a111-45ce-8f2f-12a7b8e35747",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e418c8c5-26a8-409f-a4d5-916035469168",
    "visible": true
}