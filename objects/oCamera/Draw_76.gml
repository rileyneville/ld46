/// @description 
//draw_clear_alpha(c_black,0);
//gpu_set_colorwriteenable(1,1,1,0);
//gpu_set_colorwriteenable(1,1,1,1);

if (!surface_exists(fg_surface)) {
	fg_surface = surface_create(base_camera_w,base_camera_h);
	view_set_surface_id(0,fg_surface);
}

surface_set_target(fg_surface);
draw_clear_alpha(c_black,0);
surface_reset_target();
