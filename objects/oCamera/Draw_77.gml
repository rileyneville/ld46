/// @description Draw application layer with gamma
if (!surface_exists(fg_surface)) {
	fg_surface = surface_create(base_camera_w,base_camera_h);
	view_set_surface_id(0,fg_surface);
}
shader_set(sh_gamma);
shader_set_uniform_f(u_gamma,gamma);

draw_surface_ext(fg_surface,-frac(x)*game_scale,-frac(y)*game_scale,game_scale,game_scale,0,c_white,1);
shader_reset();