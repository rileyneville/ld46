{
    "id": "2f2616f8-11f8-454e-901a-d70dbc31c0a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFence",
    "eventList": [
        {
            "id": "aeedb105-b463-49fa-a31c-9a5cce66e535",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2f2616f8-11f8-454e-901a-d70dbc31c0a0"
        },
        {
            "id": "e865d747-9ebc-4765-a39b-7c4906ae6e88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f2616f8-11f8-454e-901a-d70dbc31c0a0"
        },
        {
            "id": "62f644cc-ba3a-43e9-a66a-96a854f8429d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "2f2616f8-11f8-454e-901a-d70dbc31c0a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "72c2ae23-cb42-4468-a30a-f1260d337c57",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e955b8e9-170c-46fe-a77a-d8e7861a7380",
    "visible": true
}