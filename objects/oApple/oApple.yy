{
    "id": "a2a5bab1-7f83-4590-a801-208566da2f5b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oApple",
    "eventList": [
        {
            "id": "bd092c04-046e-4654-8ae0-46dfdb388217",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a2a5bab1-7f83-4590-a801-208566da2f5b"
        },
        {
            "id": "f3ec745e-4880-49da-874f-bf3d93e4ab11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a2a5bab1-7f83-4590-a801-208566da2f5b"
        },
        {
            "id": "b3c47d1e-0b18-426e-96b6-1793ee713ae7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a2a5bab1-7f83-4590-a801-208566da2f5b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ae9177f6-a196-4012-a1f5-a8b676c81991",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e6d55551-e823-4893-b568-c76b46c8579d",
    "visible": true
}