{
    "id": "2f37e8c1-65ea-4c87-97fb-a13d54c3cf9b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStone",
    "eventList": [
        {
            "id": "82c102ac-11eb-4ade-9bbe-70e6a9234035",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2f37e8c1-65ea-4c87-97fb-a13d54c3cf9b"
        },
        {
            "id": "550f931e-f88b-45e7-847f-a8bec8483119",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f37e8c1-65ea-4c87-97fb-a13d54c3cf9b"
        },
        {
            "id": "6cc59809-8172-4ba9-ab55-06c5cd3723cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "2f37e8c1-65ea-4c87-97fb-a13d54c3cf9b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "72c2ae23-cb42-4468-a30a-f1260d337c57",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5b762376-d939-4e96-9a6c-6a3e40caf0ba",
    "visible": true
}