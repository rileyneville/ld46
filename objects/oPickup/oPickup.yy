{
    "id": "ae9177f6-a196-4012-a1f5-a8b676c81991",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPickup",
    "eventList": [
        {
            "id": "f5f3d54d-b851-474b-a00d-1e51935c0fbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ae9177f6-a196-4012-a1f5-a8b676c81991"
        },
        {
            "id": "edaa5ea6-9890-4589-8c44-b151a040d463",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ae9177f6-a196-4012-a1f5-a8b676c81991"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d10870e2-8f54-4a96-a8a7-5de38663e72d",
    "visible": true
}