{
    "id": "e1ef5697-f084-4104-8e7a-724adebb3a93",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChicken",
    "eventList": [
        {
            "id": "86d52751-3740-4fcb-8b81-8cf23e2a2c87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e1ef5697-f084-4104-8e7a-724adebb3a93"
        },
        {
            "id": "64ca3cce-b1b8-48d0-bfa7-e46014965014",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e1ef5697-f084-4104-8e7a-724adebb3a93"
        },
        {
            "id": "19bca3c2-eed9-4b59-b07f-bdad043c56cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e1ef5697-f084-4104-8e7a-724adebb3a93"
        },
        {
            "id": "86056567-dac2-4841-97e9-4fbe465444a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e1ef5697-f084-4104-8e7a-724adebb3a93"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ae9177f6-a196-4012-a1f5-a8b676c81991",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b35c74e3-1ae2-4882-9c2b-6db7d8a4ef19",
    "visible": true
}