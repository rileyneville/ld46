/// @description 

// Inherit the parent event
event_inherited();
radius = 2;
value = 80;
hsp = 0;
vsp = 0;
acc = 0.05;
run_spd = 1;
stand_sprite = sChicken;
run_sprite = sChicken_run;