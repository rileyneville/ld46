{
    "id": "005c5dba-fd5e-4e38-aa81-4f03a4618500",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameManager",
    "eventList": [
        {
            "id": "77b91e57-f083-4674-97f4-3d6540b87f54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "005c5dba-fd5e-4e38-aa81-4f03a4618500"
        },
        {
            "id": "570f87d4-9a2f-4bd0-bd4e-5aede83abf0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "005c5dba-fd5e-4e38-aa81-4f03a4618500"
        },
        {
            "id": "a0ad792d-ffc8-4626-9b1f-21b1e3cdc0dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "005c5dba-fd5e-4e38-aa81-4f03a4618500"
        },
        {
            "id": "cd182143-3013-4259-82f7-d826c82522b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "005c5dba-fd5e-4e38-aa81-4f03a4618500"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c93babb7-a111-45ce-8f2f-12a7b8e35747",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}