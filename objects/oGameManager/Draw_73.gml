/// @description 
gpu_set_blendmode(bm_subtract);
gpu_set_colorwriteenable(1,1,1,0);
	draw_sprite_ext(sLightFalloff,0,global.light_x,global.light_y,1,1,0,c_white,1-(global.light_intensity-1));
	
gpu_set_colorwriteenable(1,1,1,1);
gpu_set_blendmode(bm_normal);
if (global.light_intensity < 1) {
	draw_sprite_ext(sLightFalloff,1,global.light_x,global.light_y,1,1,0,c_white,1-global.light_intensity);
}