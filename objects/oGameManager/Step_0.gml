/// @description 
if (keyboard_check(vk_control)) {
	if (keyboard_check_pressed(ord("R"))) {
		audio_stop_all();
		game_restart();
	}
}
/*
var total_mass = 0;
with (oPickup) {
	total_mass += value;
}
show_debug_message("Total Mass in World:" + string(total_mass));
*/
if (point_distance(oCamera.x,oCamera.y,last_activated_x,last_activated_y) > 150) {
	last_activated_x = oCamera.x;
	last_activated_y = oCamera.y;
	instance_activate_all();
	instance_deactivate_region(oCamera.x-320,oCamera.y-320,oCamera.x+320,oCamera.y+320,false,true);
	instance_activate_object(oRequired);
}