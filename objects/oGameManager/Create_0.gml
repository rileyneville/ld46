/// @description 
#macro c_text $89C0DF
#macro c_blood $0500CC
draw_set_font(fnt1);

global.room_min_x = 0;
global.room_max_x = 2160;
global.room_min_y = 3552;
global.room_max_y = room_width;

global.light_x = x;
global.light_y = y;
global.light_height = 32;
global.light_intensity = 0;
//show_debug_overlay(true);
global.ps_ground = part_system_create();
part_system_depth(global.ps_ground,690);

global.ps_air = part_system_create();
part_system_depth(global.ps_air,290);

global.pt_gore = part_type_create();
part_type_sprite(global.pt_gore,sGore,false,false,true);
part_type_color3(global.pt_gore,c_blood,c_maroon,c_maroon);
part_type_life(global.pt_gore,2000,3000);
part_type_alpha3(global.pt_gore,1,1,0);
part_type_speed(global.pt_gore,0.1,1,-0.025,0);
part_type_orientation(global.pt_gore,0,0,0,0,true);



global.pt_leaves = part_type_create();
part_type_sprite(global.pt_leaves,sLeaves,false,false,true);
part_type_color3(global.pt_leaves,c_white,c_white,c_dkgray);
part_type_life(global.pt_leaves,2000,3000);
part_type_alpha3(global.pt_leaves,1,1,0);
part_type_speed(global.pt_leaves,0.1,1,-0.025,0);
part_type_orientation(global.pt_leaves,0,360,0,0,true);


global.pt_hay = part_type_create();
part_type_sprite(global.pt_hay,sHay,false,false,true);
part_type_color3(global.pt_hay,c_white,c_white,c_dkgray);
part_type_life(global.pt_hay,2000,3000);
part_type_alpha3(global.pt_hay,1,1,0);
part_type_speed(global.pt_hay,0.1,1,-0.025,0);
part_type_orientation(global.pt_hay,0,360,0,0,true);

global.pt_rubble = part_type_create();
part_type_sprite(global.pt_rubble,sRubble,false,false,true);
part_type_color3(global.pt_rubble,c_white,c_white,c_dkgray);
part_type_life(global.pt_rubble,2000,3000);
part_type_alpha3(global.pt_rubble,1,1,0);
part_type_speed(global.pt_rubble,0.1,1,-0.025,0);
part_type_orientation(global.pt_rubble,0,360,0,0,true);

global.pt_splinters = part_type_create();
part_type_sprite(global.pt_splinters,sSplinters,false,false,true);
part_type_color3(global.pt_splinters,c_white,c_white,c_dkgray);
part_type_life(global.pt_splinters,2000,3000);
part_type_alpha3(global.pt_splinters,1,1,0);
part_type_speed(global.pt_splinters,0.1,1,-0.025,0);
part_type_orientation(global.pt_splinters,0,360,0,0,true);

last_activated_x = -1000;
last_activated_y = -1000;