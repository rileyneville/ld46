{
    "id": "8bf54f7d-081d-4b13-ad7a-6be23e0fc78d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSheep",
    "eventList": [
        {
            "id": "12168318-2c96-424a-a03f-93da3da26436",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8bf54f7d-081d-4b13-ad7a-6be23e0fc78d"
        },
        {
            "id": "6e9a1e59-6f10-4145-80dd-9771709684e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8bf54f7d-081d-4b13-ad7a-6be23e0fc78d"
        },
        {
            "id": "4d6287cc-4cd4-41f0-8900-5db1e023e6d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8bf54f7d-081d-4b13-ad7a-6be23e0fc78d"
        },
        {
            "id": "037da137-6e9e-499b-b47a-9e916b20d6db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8bf54f7d-081d-4b13-ad7a-6be23e0fc78d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "24eb0ec5-ae3a-4e16-8f32-cb9bb23b6c21",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6627b380-6554-4a5f-b3f7-568631d9264d",
    "visible": true
}