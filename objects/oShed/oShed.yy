{
    "id": "d367814e-f16a-4bbc-b51b-34c0b309f825",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShed",
    "eventList": [
        {
            "id": "8b5f3d76-38e3-4986-ab81-0501727cbfda",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d367814e-f16a-4bbc-b51b-34c0b309f825"
        },
        {
            "id": "46ec6dfb-39cf-4d27-acc2-9f5cfb6129ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d367814e-f16a-4bbc-b51b-34c0b309f825"
        },
        {
            "id": "24ad5dc8-3599-4c90-9c46-0864bdde8658",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d367814e-f16a-4bbc-b51b-34c0b309f825"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "72c2ae23-cb42-4468-a30a-f1260d337c57",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8df72964-4073-43fe-941b-57aa59dde1c9",
    "visible": true
}